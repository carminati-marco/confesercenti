'''
Created on Apr 9, 2011

@author: 
'''
from django.contrib import admin
from courses.models import Course, CourseType

from django import forms
from tinymce.widgets import TinyMCE

class CourseAdminModelForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label = ('Testo'))

    class Meta:
        model = Course


class CourseAdmin(admin.ModelAdmin):

    list_filter = ('site', 'type', )
    list_display = ('title', 'type', 'user', 'created_at', 'published', 'order', 'pub_date')
    search_fields = ['body', 'title']
    filter_horizontal = ('category', 'macrocategory',)
    raw_id_fields = ('documents',)
    
    form = CourseAdminModelForm    
    
    
    def save_model(self, request, obj, form, change):
        """ Autofill in author when blank on save models. """
        obj.user = request.user
        obj.save()
    # EndDef


admin.site.register(Course, CourseAdmin)
admin.site.register(CourseType)
