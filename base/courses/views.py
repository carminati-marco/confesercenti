from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from courses.models import Course



from django.http import Http404

from django.contrib.auth.decorators import login_required

from django.db.models import Q
from django.conf import settings

def listing(request, page=None):
    courses_list = Course.pubs_objects.all().order_by('type__order')

    return render_to_response('courses/courses_listing.html',
        {
         'courses': courses_list,
        },
        context_instance=RequestContext(request))
    
def course_details(request, slug=None, page_id=None):
    courses = get_object_or_404(Course, slug=slug)
    
    ##nel caso in cui visits sia null
    if not courses.visits:
        courses.visits = 0
        
    courses.visits = courses.visits + 1
    courses.save()
    
    if courses.published != True:
        raise Http404
    
    return render_to_response('courses/courses_detail.html',
        {
         'course': courses,         
        },
        context_instance=RequestContext(request))
    
