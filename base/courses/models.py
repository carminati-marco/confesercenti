# encoding: utf-8
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site, SiteManager
from datetime import datetime
from categories.models import Category, SubCategory
from common.thumbs import ImageWithThumbsField

from django.conf import settings

from autoslug import AutoSlugField

from documents.models import Document

class PubDateCourseManager(models.Manager):
    def get_query_set(self):
        return super(PubDateCourseManager, self).get_query_set().filter(published = True, 
                                                                        pub_date__lte = datetime.now(),
                                                                        site__id = settings.SITE_ID)

class CourseType(models.Model):
    name = models.CharField(max_length=255, verbose_name = _('Nome'))
    order = models.IntegerField(default = 0, verbose_name = _('Posizione Corso'))
    
    def __unicode__(self):
        return u"%s" % self.name
    
    class Meta:
        verbose_name = _("Tipo Corso")
        verbose_name_plural = _("Tipi Corsi")
        



class Course(models.Model):
    #pubs_objects = PublishedCourseManager()
    objects = models.Manager()
    pubs_objects = PubDateCourseManager()
    
    
    title = models.CharField(max_length=255, verbose_name = _('Titolo')) 
#    slug = models.SlugField(unique=True)
    slug = AutoSlugField(populate_from='title')
    body = models.TextField(verbose_name = _('Testo'))
    published = models.BooleanField(default=False, verbose_name = _('Pubblicato'))
    
    pub_date = models.DateTimeField(verbose_name = _('Data di pubblicazione'), default = datetime.now)
    site = models.ManyToManyField(Site, verbose_name = _('Sito'))
    macrocategory = models.ManyToManyField(Category, verbose_name = _('Macro Categoria'), blank = True, null = True)
    category = models.ManyToManyField(SubCategory, verbose_name = _('Categoria'), blank = True, null = True)
    
    type = models.ForeignKey(CourseType, verbose_name = _('Tipo Corso'))
    
    visits = models.IntegerField(default = 0, verbose_name = _('Numero visite'))
    
    order = models.IntegerField(default = 0, verbose_name = _('Posizione Corso'))
    
    # Automatic, verbose_name = _('Categoria')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    
    documents = models.ManyToManyField(Document, blank = True, null = True)
    
    image =  ImageWithThumbsField(upload_to='courses/images', sizes=((125,125),(200,200),(500,220)),
                                  verbose_name = _('Immagine'))
    
    def __unicode__(self):
        return u"%s" % self.title
    
    def get_absolute_url(self):
        return "/corsi_confe/dettaglio/%s/" % self.slug
    
    class Meta:
        ordering = ['order', '-pub_date']
        verbose_name = _("Corso")
        verbose_name_plural = _("Corsi")

