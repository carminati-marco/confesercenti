from django.conf.urls.defaults import *


urlpatterns = patterns('courses.views',
    url(r'^$', 'listing', name='list-courses'),
    url(r'^dettaglio/(?P<slug>[-\w]+)/$', 'course_details', name='course-details'),
        
)
