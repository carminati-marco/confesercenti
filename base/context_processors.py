from django.conf import settings
from django.contrib.sites.models import Site


def news_context(request):
    return {
            'NEWS_NEWS_TYPE_ID': settings.NEWS_NEWS_TYPE_ID,
            'NEWS_COURSES_TYPE_ID' : settings.NEWS_COURSES_TYPE_ID,
            'NEWS_PROMO_TYPE_ID' : settings.NEWS_PROMO_TYPE_ID,
            'NEWS_CONV_TYPE_ID' : settings.NEWS_CONV_TYPE_ID,
            'NEWS_ANNOUNCEMENT_TYPE_ID' : settings.NEWS_ANNOUNCEMENT_TYPE_ID,
            'NEWS_VISITS_ORDER_BY' : settings.NEWS_VISITS_ORDER_BY,
            "NEWS_PUB_DATE_ORDER_BY" : settings.NEWS_PUB_DATE_ORDER_BY,
            'site': Site.objects.get_current()

            }