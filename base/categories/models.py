from django.db import models
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site, SiteManager
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField
from common.thumbs import ImageWithThumbsField
from django.utils.translation import ugettext_lazy as _

##inserire la traduzione _ sui verbose_name

##Categoria, viene utilizzata da diversi modelli e siti.
##Puo' essere associata a piu' siti
class Category(models.Model):
    name = models.CharField(max_length = 200, verbose_name = ('Nome'))
    slug = AutoSlugField(populate_from='name')
    description = models.CharField(max_length = 200, verbose_name = ('Descrizione'))
    sites = models.ManyToManyField(Site)
    
    logo =  ImageWithThumbsField(upload_to='images/categories', sizes=((120,120),(50,50) ), blank = True, null=True)
    order = models.IntegerField(verbose_name = _('No. Ordine'))
    link = models.URLField(verbose_name = _('Percorso Link'))
    
    objects = models.Manager()
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = _('Categoria')
        verbose_name_plural = _('Categorie')        
        ordering  = ('order',)
        get_latest_by = 'order' 
        
    def public_name(self):
        return self.name   
        
    
    def get_absolute_url(self):
#        print "minchia"        
#        print reverse("categories_detail", args= [self.slug ])
        return reverse("categories_detail", args= [self.slug ])
    
    def get_subcategories(self):
        return SubCategory.objects.filter(sites = Site.objects.get_current(), category = self)
    
    def get_sites(self):
        sites = self.sites.all()
        return_list = [] 
        for site in sites:
            return_list.append(site.name)
        
        return return_list

class SubCategory(models.Model):
    name = models.CharField(max_length = 200, verbose_name = ('Nome'))
    public_name = models.CharField(max_length = 200, verbose_name = ('Nome Pubblico'))
    slug = AutoSlugField(populate_from='name')
    description = models.CharField(max_length = 200, verbose_name = ('Descrizione'))
    category = models.ForeignKey(Category, verbose_name = ('categoria'))
    sites = models.ManyToManyField(Site)
    
    logo =  ImageWithThumbsField(upload_to='images/categories', sizes=((120,120),(50,50)), blank = True, null=True)    
    order = models.IntegerField(verbose_name = _('No. Ordine'))
    link = models.URLField(verbose_name = _('Percorso Link'), blank = True, null=True)

    ##campi aggiunti.    
    president_logo =  ImageWithThumbsField(upload_to='images/president', sizes=((120,120),(50,50)), blank = True, null=True, verbose_name = ('Foto Presidente'))
    president_name = models.CharField(max_length = 50, verbose_name = ('Nome Presidente'), blank = True, null=True)
    
    responsible_name = models.CharField(max_length = 50, verbose_name = ('Responsabile sindacale'), blank = True, null=True)
    
    
    phone = models.CharField(max_length = 20, verbose_name = ('Telefono'), blank = True, null=True)
    email = models.CharField(max_length = 50, verbose_name = ('E-mail'), blank = True, null=True)
    fax = models.CharField(max_length = 20, verbose_name = ('Fax'), blank = True, null=True)
    
    objects = models.Manager()
    site_objects = SiteManager()    
    
    def __unicode__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse("subcategories_detail", args= [self.slug ])
    
    def get_sites(self):
        sites = self.sites.all()
        return_list = [] 
        for site in sites:
            return_list.append(site.name)
        
        return return_list
    
    class Meta:
        verbose_name = _('SottoCategoria')
        verbose_name_plural = _('SottoCategorie')        
        ordering  = ('name',)
        get_latest_by = 'order'    