from django.contrib import admin
from categories.models import Category, SubCategory

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_sites',)
    list_filter = ('sites',)
    

admin.site.register(Category, CategoryAdmin)

class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name','category', 'public_name', 'get_sites',)
    search_fields = ['name', 'public_name',]
    ordering = ('-category',)
    list_filter = ('category', 'sites',)
    

admin.site.register(SubCategory, SubCategoryAdmin)