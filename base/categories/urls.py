from django.conf.urls.defaults import *


urlpatterns = patterns('categories.views',


    url(r'^$',
        view='home',
        name='categories_home'
    ),
    
    url(r'^categoria/(?P<slug>[-\w]+)/$',
        view='category_list',
        name='category_category_list'
    ),                           
                       
    url(r'^dettaglio/(?P<slug>[-\w]+)/$',
        view='detail',
        name='categories_detail'
    ),
    
    url(r'^dettaglio/sotto_categoria/(?P<slug>[-\w]+)/$',
        view='detail_subcategory',
        name='subcategories_detail'
    ),
)
