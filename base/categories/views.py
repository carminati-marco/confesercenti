import datetime
import re

from django.contrib.sites.models import Site
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import Http404
from django.views.generic import date_based, list_detail
from django.db.models import Q
from django.conf import settings

from categories.models import *



def home(request, template_name = 'categories/category_list.html', **kwargs):
    
    
    
    return render_to_response(template_name,
          {
           "categories" : Category.objects.filter(sites = Site.objects.get_current())
           }
          ,
          context_instance=RequestContext(request))
    
def category_list(request, slug, template_name = 'categories/category_slug_list.html', **kwargs):
    
    return render_to_response(template_name,
          {
           "categories" : Category.objects.filter(sites = Site.objects.get_current(), slug = slug)
           }
          ,
          context_instance=RequestContext(request))        


def detail(request, slug, template_name = 'categories/category_detail.html',):
    
    category = Category.objects.get(slug = slug)
    
    return render_to_response(template_name,
      {
       "category" : category
       }
      ,
      context_instance=RequestContext(request))    
    

def detail_subcategory(request, slug, template_name = 'categories/subcategory_detail.html',):
    
    category = SubCategory.objects.get(slug = slug, sites = Site.objects.get_current())
    
    return render_to_response(template_name,
      {
       "category" : category
       }
      ,
      context_instance=RequestContext(request))  
