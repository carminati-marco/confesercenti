module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            compile: {
                files: {
                    
//                    '../static/assets/css/main.css': 'src/css/bootstrap.less',
                    '../static/assets/css/custom.css': 'src/css/custom.less'
                }
            }

        },
        concat: {
            js: {
                src: ['node_modules/jquery/dist/jquery.min.js',
                      'node_modules/bootstrap/dist/js/bootstrap.min.js',
                      'node_modules/masonry-layout/dist/masonry.pkgd.min.js'
                      //,
//                      'node_modules/modernizr/dist/modernizr-build.min.js'
                      ],
                dest: '../static/assets/js/main.js'
            },
            js2: {
                src: ['src/**/*.js'],
                dest: '../static/assets/js/custom.js'
            }
//            },
//            css: {
//                src: 'node_modules/bootstrap/dist/css/bootstrap.min.css',
//                dest: '../static/assets/css/main.css'
            
        },
        
       
        watch: {
            css: {
                files: [
                    'src/**/*.js', 
                    'src/css/**/*.less'
                ],
                tasks: ['less', 'concat'],
            },
        }
    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');



    grunt.registerTask('default', ['less', 'concat', 'watch']);

};