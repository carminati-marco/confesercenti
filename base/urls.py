from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^base/', include('base.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
     #(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
     (r'^admin/', include(admin.site.urls)),
     (r'^photologue/', include('photologue.urls')),
     
     (r'^accounts/login/$', 'django.contrib.auth.views.login'),
     (r'^accounts/logout/$', 'django.contrib.auth.views.logout'),
     
     

     
     ## Comments.
     (r'^comments/', include('django.contrib.comments.urls')),
     (r'^news/', include('news.urls')),
     (r'^corsi_confe/', include('courses.urls')),
     (r'^convenzioni_confe/', include('conventions.urls')),
     (r'^contact_form/', include('contact_form.urls')),
     
     (r'^basic_place/', include('basic.places.urls')),
     (r'^corsi/', include('basic.events.urls')),
     
     (r'^pubblicazioni/', include('documents.urls')),
     
     (r'^static/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': '/Users/Alien/Documents/Aptana Studio 3 Workspace/quaranta_cm/trunk/base/static/'}),

#     (r'^search/', include('haystack.urls')),
     
     
     (r'^associazionidicategoria/', include('categories.urls')),
     
     ##home deve rimnere ultima, perche' risponde a tutti gli indirizzi, serve per evitare di far apparire una pagina di errore
     ##nel caso di indirizzo non corretto     
     
     
    url(r'^$', 'news.views.home', name='home'),
    (r'^registration/', include('registration.urls')),
    (r'^bandi/', include('announcement.urls')),
    
    
    url(r'^servizi/formazione/$', 'news.views.redirect_cescot', name='news_redirect_cescot'),
    url(r'^develop/$', 'news.views.home_develop', name='home_develop'),     
    (r'^cescot/', include('blog.urls')),
)
