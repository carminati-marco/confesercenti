# Django settings for base project.

DEBUG = False
TEMPLATE_DEBUG = DEBUG

import os
DIRNAME = os.path.dirname(__file__)
print DIRNAME

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

EMAIL_HOST = 'mail.conf.bg.it'
EMAIL_HOST_USER = 'cescot@conf.bg.it'
#EMAIL_HOST_PASSWORD = '!Conf04'
EMAIL_HOST_PASSWORD = 'G9j3H0cF'
EMAIL_HOST_PASSWORD = 'C68d6aSDqFKl'
EMAIL_PORT = 25
#EMAIL_USE_TLS = True


DEFAULT_EMAIL_ANNOUNCEMENT = 'f.caselli@conf.bg.it'
DEFAULT_EMAIL_INFO  = ['info@conf.bg.it']
DEFAULT_EMAIL_CESCOT = 'cescot@conf.bg.it'
DEFAULT_EMAIL = 'cescot@conf.bg.it'

# DEFAULT_EMAIL_INFO = 'info@conf.bg.it'

DJANGO_PROJECT = 'base'

MANAGERS = ADMINS

"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'confesercenti',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'password',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
"""
DATABASES = {
     'default': {
         'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
         'NAME': 'quarantacm_confesercenti',                      # Or path to database file if using sqlite3.
         'USER': 'quarantacm',                      # Not used with sqlite3.
         'PASSWORD': 'US3HtTCO',                  # Not used with sqlite3.
         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
     }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Rome'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'IT-it'


# PAGE_NEWS_ID = 1
# SITE_ID = 1
PAGE_NEWS_ID = 2
SITE_ID = 3

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(DIRNAME, 'static/')
print 'MEDIA_ROOT'
print MEDIA_ROOT


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL="/static/"


# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'p#m$$gyxubhsuk%#-s0yx!#11p44@dmvwc0a3j_=$48^sap522'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.csrf.middleware.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'pagination.middleware.PaginationMiddleware',    
)

import django.conf.global_settings as DEFAULT_SETTINGS
TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
                                                                              "context_processors.news_context",
                                                                              "django.core.context_processors.request",
                                                                              )


ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    # os.path.join(DIRNAME,'templates', ),
   os.path.join(DIRNAME,'templates_responsive_milano', ),
   os.path.join(DIRNAME,'templates_responsive', ),

)

print TEMPLATE_DIRS

##Setup TinyMce
TINYMCE_JS_URL = os.path.join(MEDIA_URL + 'js/tiny_mce/tiny_mce.js')
TINYMCE_JS_ROOT= os.path.join(MEDIA_URL, 'js/tiny_mce')
TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
    'cleanup_on_startup': False,
    'custom_undo_redo_levels': 10,    
    'force_br_newlines' : False,
    'force_p_newlines' : True,
    'forced_root_block' : '',
    
    'extended_valid_elements' : "iframe[src|width|height|name|align]",
    
}



TINYMCE_SPELLCHECKER = False
TINYMCE_COMPRESSOR = False


MODULES_DOCUMENT_ID = 4

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.comments',    
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    # Uncomment the next line to enable the admin:
     'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
     'django.contrib.admindocs',
     
    'django.contrib.flatpages',
    'django.contrib.markup',
    
    'registration',
    
    
    'tinymce',     
    # qta library
    'flatpages_extended',
    'common',
    'photologue',
    'documents',
    'banner',
    ##django-basic-app
    'tagging',
    'dateutil',
    'basic.places',
    'basic.events',    
    'news',
    'categories',
    'contact_form',
    'uni_form',
    'announcement',
        
    'pagination',
    'courses',
    'conventions',
    'blog',

    'cover',
    'comments_extended',

    'info',
)

COMMENTS_APP = 'comments_extended'

import logging
import logging.handlers

LOG_FILENAME = 'base_log.out'

# Set up a specific logger with our desired output level
MY_LOGGER = logging.getLogger('MyLogger')
MY_LOGGER.setLevel(logging.DEBUG)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILENAME, maxBytes=20000, backupCount=5)

MY_LOGGER.addHandler(handler)
MY_LOGGER.debug('settings.py')

"""haystack"""
#HAYSTACK_SITECONF = DIRNAME + 'search_indexes'
#HAYSTACK_SITECONF = 'base.search_indexes'
#HAYSTACK_SEARCH_ENGINE = 'dummy'

NEWS_NEWS_TYPE_ID = 1
NEWS_COURSES_TYPE_ID = 2
NEWS_PROMO_TYPE_ID = 3
NEWS_CONV_TYPE_ID = 4
NEWS_ANNOUNCEMENT_TYPE_ID = 5
NEWS_VISITS_ORDER_BY = "-visits"
NEWS_PUB_DATE_ORDER_BY = "-pub_date"
NEWS_PER_TAG = 4

FORCE_SCRIPT_NAME = ""

"""registration"""
ACCOUNT_ACTIVATION_DAYS = 7



