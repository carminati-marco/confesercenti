import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.contrib.auth.models import User
from tagging.fields import TagField
from basic.places.models import Place
from autoslug import AutoSlugField


class Event(models.Model):
    """Event model"""
    title = models.CharField(max_length=200, verbose_name = _('Titolo'))
    slug = AutoSlugField(populate_from='title')
    place = models.ForeignKey(Place, blank=True, null=True)
    one_off_place = models.CharField(max_length=200, blank=True, verbose_name = _('Sotto Titolo'))
    description = models.TextField(blank=True, verbose_name = _('Descrizione'))
    submitted_by = models.ForeignKey(User, blank=True, null=True, verbose_name = _('Promosso da'))
    tags = TagField()
    created = models.DateTimeField(auto_now_add=True, verbose_name = _('Creato'))
    modified = models.DateTimeField(auto_now=True, verbose_name = _('Modificato'))
    
    responsible = models.CharField(max_length=200, verbose_name = _('Responsabile'))
    email = models.EmailField()
    phone = models.CharField(max_length=20, verbose_name = _('Telefono'), blank=True)
    fax = models.CharField(max_length=20, verbose_name = _('Fax'), blank=True)

     

    class Meta:
        verbose_name = _('Evento')
        verbose_name_plural = _('Eventi')
        db_table = 'events'

    def __unicode__(self):
        return self.title


class EventTime(models.Model):
    """EventTime model"""
    event = models.ForeignKey(Event, related_name='event_times')
    start = models.DateTimeField(verbose_name = _('Inizio'))
    end = models.DateTimeField(blank=True, null=True, verbose_name = _('Fine'))
    is_all_day = models.BooleanField(default=False, verbose_name = _('Giornata Intera'))

    class Meta:
        verbose_name = _('Tempo')
        verbose_name_plural = _('Tempi')
        db_table = 'event_times'

    @property
    def is_past(self):
        NOW = datetime.date.now()
        if self.start < NOW:
            return True
        return False

    def __unicode__(self):
        return u'%s' % self.event.title

    @permalink
    def get_absolute_url(self):
        return ('event_detail', None, {
            'year': self.start.year,
            'month': self.start.strftime('%b').lower(),
            'day': self.start.day,
            'slug': self.event.slug,
            'event_id': self.event.id
        })