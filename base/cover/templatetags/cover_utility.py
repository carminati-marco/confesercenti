from django import template
from django.conf import settings

from django.template.loader import render_to_string
from cover.models import Cover

register = template.Library()



class CoverDetailCountNode(template.Node):
    def __init__(self, no_item):
        self.no_item = template.Variable(no_item)
        
    def render(self, context):
        no_item = self.no_item.resolve(context)            
        cover_list = Cover.valid_objects.all()[:no_item]
        
        rendered = render_to_string('cover/cover.html', { 
                                                        'cover_list' : cover_list  })
        
        return rendered


def get_cover(parser, token):
    try:
        ##split contents
        tag_name, no_item = token.split_contents()
    except ValueError, e:        
        msg = "%r tag requires a single argument" %token.split_contents()[0]
        raise template.TemplateSyntaxError(msg)
        
    return CoverDetailCountNode(no_item)

register.tag('get_cover', get_cover)