from django.db import models
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

  
class ValidCoverManager(models.Manager):
    def get_query_set(self):
        return (super(ValidCoverManager, self).get_query_set().filter(hidden = False).order_by('-created'))    


class Cover(models.Model):
       
    image = models.ImageField(upload_to='images')
    hidden = models.BooleanField(default = False, verbose_name = _('Nascosto'))    
    created = models.DateTimeField(default = datetime.now)    
        
    objects = models.Manager()
    valid_objects = ValidCoverManager()
    
    def __unicode__(self):
        return unicode(self.created)
    
    class Meta:
        verbose_name        = ('Copertina Punto Informativo')
        verbose_name_plural = ('Copertine Punto Informativo')