
from django.conf import settings
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site, SiteManager
from django.db import models
from django.utils.translation import ugettext_lazy as _




    
    
class MenuRootManager(models.Manager):
    def get_query_set(self):
        return super(MenuRootManager, self).get_query_set().filter(parent = None,
                                                                   site__id=settings.SITE_ID).order_by('order')
                                                                   
#class ExtendedFlatPageBox(models.Model):
#    name = models.CharField(_("Nome"), max_length=200)
#    description = models.CharField(_("Descrizione"), max_length=200)
#    link = models.URLField(verbose_name = _('Percorso Link'), blank = True, null=True, verify_exists=False)                                                                   
    

class Menu(models.Model):
    """
    oggetto che crea albero per il menu
    """
    site = models.ManyToManyField(Site, verbose_name=_('Sito'))
    name = models.CharField(_("Nome"), max_length=200)
    slug = models.SlugField(_("Slug"), help_text=_("Used for URLs, auto-generated from name if blank"), blank=True)
    parent = models.ForeignKey('self', blank=True, null=True,
        related_name='child')
#    extended_flat_page = models.ForeignKey(ExtendedFlatPage, blank=True, null=True,
#        related_name='extended_flat_page_menu')

    order = models.PositiveIntegerField()

    sites = models.ManyToManyField(Site, blank=True, null=True,
                                   related_name='sites_menu')

    
    
    
    objects = models.Manager()
    root_objects = MenuRootManager()
    site_objects = SiteManager() 
    
    class Meta:
        ordering = ['order']   
    
    def __unicode__(self):
        if self.parent:
            return self.parent.name + ' - ' + self.name
        else:
            return self.name

    def get_absolute_url(self):
        if self.extended_flat_page_menu:
            try:
                return self.extended_flat_page_menu.all()[0].url
            except:
                return ''
            
    def get_child(self):
        site = Site.objects.get_current()
        return self.child.filter(site = site)
        



class ExtendedFlatPage(FlatPage):
    
    menu = models.ForeignKey(Menu, blank=True, null=True,
        related_name='extended_flat_page_menu')
    


    active_box = models.BooleanField(default=True, verbose_name = _('Box Attivato'))
    responsible = models.CharField(_("Responsabile"), max_length=200, blank=True, null=True)
    info = models.CharField(_("Info"), max_length=200, blank=True, null=True)
    ##
    email_description = models.CharField(_("Descrizione Email"), max_length=200, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)    
    phone = models.CharField(max_length=20, verbose_name = _('Telefono'), blank=True, null=True)
    fax = models.CharField(max_length=20, verbose_name = _('Fax'), blank=True, null=True)    
    website = models.URLField(blank=True, null = True, verify_exists=False)    
    
    site_objects = SiteManager()
    
    
