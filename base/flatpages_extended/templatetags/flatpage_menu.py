from django import template
from flatpages_extended.models import Menu, ExtendedFlatPage

register = template.Library()

def get_menu_batch(parser, token):
    format_string = ''
    return RootMenusNode(format_string)

class RootMenusNode(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):                
        context['root_menu_list'] = Menu.root_objects.all()
        return ''

register.tag('get_root_menu', get_menu_batch)




def get_extended_flatpage_batch(parser, token):
    format_string = ''
    tag_name, arg = token.contents.split(None, 1)
    
    return ExtendedRootMenusNode(arg)


class ExtendedRootMenusNode(template.Node):
    def __init__(self, id_flatpage):
        self.id_flatpage = template.Variable(id_flatpage)
    
    def render(self, context):   
        id_flatpage = self.id_flatpage.resolve(context)
        extended_flat_page = ExtendedFlatPage.objects.get(id = id_flatpage).extendedflatpage
                             
        context['extended_flat_page'] = extended_flat_page.extendedflatpage
        return ''

register.tag('get_extended_flatpage', get_extended_flatpage_batch)