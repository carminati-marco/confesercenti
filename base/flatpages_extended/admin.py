from django.contrib import admin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage

from django import forms
from models import ExtendedFlatPage, Menu
from tinymce.widgets import TinyMCE

class ExtendedFlatPageForm(FlatpageForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},))

    class Meta:
        model = ExtendedFlatPage

class ExtendedFlatPageAdmin(FlatPageAdmin):
    list_display = ('title','url','menu')
    list_filter = ('sites', )
    search_fields = ['title','url','content',]
    
    form = ExtendedFlatPageForm
    fieldsets = (
        (None, {'fields': ('menu', 'url', 'title', 'content', 'sites', 
                           'active_box', 'responsible', 'info', 'email_description', 'email', 'phone', 'fax', 'website',)}),
    )
    
         

admin.site.unregister(FlatPage)
#admin.site.register(ExtendedFlatPage, ExtendedFlatPageAdmin)
admin.site.register(ExtendedFlatPage, ExtendedFlatPageAdmin)



class MenuAdmin(FlatPageAdmin):
    list_display = ('name', 'parent',)
    list_filter = ('site',)
admin.site.register(Menu, MenuAdmin)
