# encoding: utf-8
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site, SiteManager
from datetime import datetime
from categories.models import Category, SubCategory
from common.thumbs import ImageWithThumbsField

from autoslug import AutoSlugField

from documents.models import Document

from django.conf import settings


class PubDateBlogManager(models.Manager):
    def get_query_set(self):
        return super(PubDateBlogManager, self).get_query_set().filter(published = True, 
                                                                            pub_date__lte = datetime.now(),
                                                                            site__id = settings.SITE_ID)

class BlogType(models.Model):
    name = models.CharField(max_length=255, verbose_name = _('Nome'))
    
    def __unicode__(self):
        return u"%s" % self.name
    
    class Meta:
        ordering = ['name']
        verbose_name = _("Tipo blog")
        verbose_name_plural = _("Tipi blog")
        



class Blog(models.Model):
    #pubs_objects = PublishedBlogManager()
    objects = models.Manager()
    pubs_objects = PubDateBlogManager()
    
    
    title = models.CharField(max_length=255, verbose_name = _('Titolo')) 
#    slug = models.SlugField(unique=True)
    slug = AutoSlugField(populate_from='title')
    body = models.TextField(verbose_name = _('Testo'))
    published = models.BooleanField(default=False, verbose_name = _('Pubblicato'))
    
    pub_date = models.DateTimeField(verbose_name = _('Data di pubblicazione'), default = datetime.now)
    site = models.ManyToManyField(Site, verbose_name = _('Sito'))
    macrocategory = models.ManyToManyField(Category, verbose_name = _('Macro Categoria'), blank = True, null = True)
    category = models.ManyToManyField(SubCategory, verbose_name = _('Categoria'), blank = True, null=True)
    
    type = models.ForeignKey(BlogType, verbose_name = _('Tipo'))
    
    visits = models.IntegerField(default = 0, verbose_name = _('Numero visite'))
    
    order = models.IntegerField(default = 0, verbose_name = _('Posizione Corso'))
    
    # Automatic, verbose_name = _('Categoria')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    
    image =  ImageWithThumbsField(upload_to='conventions/images', sizes=((125,125),(200,200),(500,220)),
                                  verbose_name = _('Immagine'))
    
    link = models.URLField(verbose_name = _('Percorso Link'), blank = True, null=True)
    phone = models.CharField(max_length = 20, verbose_name = ('Telefono'), blank = True, null=True)
    email = models.CharField(max_length = 50, verbose_name = ('E-mail'), blank = True, null=True)
    fax = models.CharField(max_length = 20, verbose_name = ('Fax'), blank = True, null=True)
    
    documents = models.ManyToManyField(Document, blank = True, null = True)    
    
    def __unicode__(self):
        return u"%s" % self.title
    
    def get_absolute_url(self):
        return "/cescot/dettaglio/%s/" % self.slug
    
    class Meta:
        ordering = ['order', '-pub_date']
        verbose_name = _("Post ")
        verbose_name_plural = _("Post ")

