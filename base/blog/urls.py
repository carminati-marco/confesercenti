from django.conf.urls.defaults import *


urlpatterns = patterns('blog.views',
    url(r'^$', 'listing', name='list-blog'),
    url(r'^dettaglio/(?P<slug>[-\w]+)/$', 'blog_details', name='blog-details'),
        
)
