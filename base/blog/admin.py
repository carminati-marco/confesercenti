'''
Created on Apr 9, 2011

@author: 
'''
from django.contrib import admin
from .models import Blog, BlogType

from django import forms
from tinymce.widgets import TinyMCE

class BlogAdminModelForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label = ('Testo'))

    class Meta:
        model = Blog


class BlogAdmin(admin.ModelAdmin):    
    list_display = ('title','user', 'created_at', 'published', 'order', 'pub_date')
    search_fields = ['body', 'title']
    filter_horizontal = ('category', 'macrocategory',)
    raw_id_fields = ('documents',)
    
    form = BlogAdminModelForm
    
    def save_model(self, request, obj, form, change):
        """ Autofill in author when blank on save models. """
        obj.user = request.user
        obj.save()
    # EndDef


admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogType)
