from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from django.http import Http404

from .models import Blog


def listing(request, page=None):
    blog_list = Blog.pubs_objects.all().order_by('type__name')



    return render_to_response('blog/blog_listing.html',
                              {
                                  'blog_list': blog_list,
                              },
                              context_instance=RequestContext(request))


def blog_details(request, slug=None, page_id=None):
    blog = get_object_or_404(Blog, slug=slug)

    ##nel caso in cui visits sia null
    if not blog.visits:
        blog.visits = 0

    blog.visits = blog.visits + 1
    blog.save()

    if blog.published != True:
        raise Http404

    return render_to_response('blog/blog_detail.html',
                              {
                                  'blog': blog,
                              },
                              context_instance=RequestContext(request))
    
