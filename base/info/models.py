from datetime import datetime
from django.conf import settings
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _

from autoslug import AutoSlugField


class PubInfoManager(models.Manager):
    def get_query_set(self):
        return super(PubInfoManager, self).get_query_set().filter(published=True, site__id=settings.SITE_ID)


class Info(models.Model):

    objects = models.Manager()
    pubs_objects = PubInfoManager()


    title = models.CharField(max_length=255, verbose_name=_('Titolo'))
    slug = AutoSlugField(populate_from='title')
    body = models.TextField(verbose_name = _('Testo'))
    published = models.BooleanField(default=False, verbose_name = _('Pubblicato'))

    site = models.ManyToManyField(Site, verbose_name = _('Sito'))

    visits = models.IntegerField(default=0, verbose_name = _('Numero visite'))
    order = models.IntegerField(default=0, verbose_name = _('Ordine'))


    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"%s" % self.title

    class Meta:
        ordering = ['order',]
        verbose_name = _("Informazione")
        verbose_name_plural = _("Informazioni")

