from django.contrib import admin
from contact_form.models import SentMail, NewsletterMail

admin.site.register(SentMail)
admin.site.register(NewsletterMail)