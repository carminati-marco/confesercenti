from django.conf import settings
from django.contrib.sites.models import Site, SiteManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

class SentMail(models.Model):
    name = models.CharField(max_length=100, verbose_name = _('Nome'))
    email = models.EmailField(verbose_name = _('e-mail'))
    body = models.CharField(max_length=1000, verbose_name = _('Testo'), null = True, blank = True)
    
    age = models.IntegerField(verbose_name = _('Eta'), null = True, blank = True)
    telephone_number = models.CharField(max_length=100, verbose_name = _('Numero di telefono'), null = True, blank = True)
    category = models.CharField(max_length=100, verbose_name = _('Categoria'), null = True, blank = True)
    istat = models.CharField(max_length=100, verbose_name = _('Istat'), null = True, blank = True)
    city = models.CharField(max_length=100, verbose_name = _('Citta'), null = True, blank = True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.email


class NewsletterMail(models.Model):
    first_name = models.CharField(max_length=100, verbose_name=_('Nome'))
    last_name = models.CharField(max_length=100, verbose_name=_('Cognome'))
    email = models.EmailField(verbose_name=_('e-mail'))

    active = models.BooleanField(default=True, verbose_name=_('Attivo'))
    created_at = models.DateTimeField(auto_now_add=True)
    site = models.ForeignKey(Site, default=settings.SITE_ID)

    def __unicode__(self):
        return self.email