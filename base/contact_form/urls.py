"""
Example URLConf for a contact form.

Because the ``contact_form`` view takes configurable arguments, it's
recommended that you manually place it somewhere in your URL
configuration with the arguments you want. If you just prefer the
default, however, you can hang this URLConf somewhere in your URL
hierarchy (for best results with the defaults, include it under
``/contact/``).

"""


from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from contact_form.views import contact_form, contact_form_analytics, contact_form_professional, \
    contact_form_course, contact_form_associate, contact_form_announcement, contact_form_info, \
    contact_form_newsletter, contact_form_newsletter_disattiva, contact_form_newsletter_excel


urlpatterns = patterns('',
                       url(r'^$',
                           contact_form,
                           name='contact_form'),
                        url(r'^analisi/$',
                           contact_form_analytics,
                           name='contact_form_analytics'),
                        url(r'^professionale/$',
                           contact_form_professional,
                           name='contact_form_professional'),
                        url(r'^corsi/$',
                           contact_form_course,
                           name='contact_form_course'),
                       url(r'^associarsi/$',
                           contact_form_associate,
                           name='contact_form_associate'),

                        url(r'^bandi/$',
                           contact_form_announcement,
                           name='contact_form_announcement'),

                        url(r'^info/$',
                           contact_form_info,
                           name='contact_form_info'),

                        url(r'^newsletter/$',
                           contact_form_newsletter,
                           name='contact_form_newsletter'),

                        url(r'^newsletter/disattiva/$',
                           contact_form_newsletter_disattiva,
                           name='contact_form_newsletter_disattiva'),

                        url(r'^newsletter/excel/$',
                           contact_form_newsletter_excel,
                           name='contact_form_newsletter_excel'),


                       url(r'^sent/$',
                           direct_to_template,
                           { 'template': 'contact_form/contact_form_sent.html' },
                           name='contact_form_sent'),


                       )
