from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site, SiteManager
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
##from photologue.models import Photo
from common.thumbs import ImageWithThumbsField
from autoslug import AutoSlugField

"""
Tipo
"""
class TypeBanner(models.Model):
    name = models.CharField(max_length = 200, verbose_name = _('Nome'))
    slug = AutoSlugField(populate_from='name')
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'))
    sites = models.ManyToManyField(Site, blank=True, null=True)
        
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.name
    
    def save(self, **kwargs): 
        self.slug = slugify(self.name)
        super(TypeBanner, self).save(**kwargs) 
    
    def get_absolute_url(self):
        return self.id
    
    class Meta:
        verbose_name = _('Tipo Banner')
        verbose_name_plural = _('Tipi Banner')        
        ordering  = ('-name',)
        get_latest_by = 'name'    
    
    
"""
Banner
"""
class Banner(models.Model):
    title = models.CharField(max_length = 200, verbose_name = _('Titolo'))
    slug = AutoSlugField(populate_from='title')
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'), blank = True, null=True)
    active = models.BooleanField(default=True, verbose_name = _('Attivato'))
    href_active = models.BooleanField(default= False, verbose_name = _('Apri in una nuova pagina'))    
#    view_photo = models.BooleanField(default=False, verbose_name = _('Vedi Foto'))
##    logo =  models.ForeignKey(Photo, related_name='banner_photo', verbose_name = _('Foto'))
    logo =  ImageWithThumbsField(upload_to='images', sizes=((300,240), (240,96),(200,100),(160,120)), blank = True, null=True)
    swf = models.FileField(upload_to='swf', blank=True, null=True)
    order = models.IntegerField(verbose_name = _('No. Ordine'))
    
    type = models.ForeignKey(TypeBanner, related_name='banner_type', verbose_name = _('Tipo'))
    link = models.URLField(verbose_name = _('Percorso Link'), blank = True, null=True)
        
    sites = models.ManyToManyField(Site)
    
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.title

    def save(self, **kwargs): 
        self.slug = slugify(self.title)
        super(Banner, self).save(**kwargs)
    
    def get_absolute_url(self):
        return self.id
    
    def get_sites(self):
        sites = self.sites.all()
        return_list = [] 
        for site in sites:
            return_list.append(site.name)
        
        return return_list
    
    class Meta:
        verbose_name = _('Banner')
        verbose_name_plural = _('Banner')        
        ordering  = ('type', 'order', 'title',)
        get_latest_by = 'name'        


class SlideBanner(models.Model):
    title = models.CharField(max_length = 200, verbose_name = _('Titolo'))
    slug = AutoSlugField(populate_from='title')
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'), blank = True, null=True)
    active = models.BooleanField(default=True, verbose_name = _('Attivato'))
    href_active = models.BooleanField(default=False, verbose_name = _('Apri in una nuova pagina'))    
#    view_photo = models.BooleanField(default=False, verbose_name = _('Vedi Foto'))
##    logo =  models.ForeignKey(Photo, related_name='banner_photo', verbose_name = _('Foto'))
    logo =  ImageWithThumbsField(upload_to='images', sizes=((240,96),(200,100),(160,120),(160,250)), blank = True, null=True)
    swf = models.FileField(upload_to='swf', blank=True, null=True)
    order = models.IntegerField(verbose_name = _('No. Ordine'))
    
    type = models.ForeignKey(TypeBanner, related_name='slide_banner_type', verbose_name = _('Tipo'))
    link = models.URLField(verbose_name = _('Percorso Link'), blank = True, null=True)
        
    sites = models.ManyToManyField(Site)
    
    site_objects = SiteManager()    
    
    description_line_1 = models.CharField(max_length = 200, verbose_name = _('Descrizione 1a Linea'), null = True, blank = True)
    link_line_1 = models.URLField(verbose_name = _('Percorso Link 1a Linea'), blank=True, null=True)
    
    description_line_2 = models.CharField(max_length = 200, verbose_name = _('Descrizione 2a Linea'), null = True, blank = True)
    link_line_2 = models.URLField(verbose_name = _('Percorso Link 2a Linea'), blank=True, null=True)
    
    description_line_3 = models.CharField(max_length = 200, verbose_name = _('Descrizione 3a Linea'), null = True, blank = True)
    link_line_3 = models.URLField(verbose_name = _('Percorso Link 3a Linea'), blank=True, null=True)
    
        
    
    class Meta:
        verbose_name = _('Banner Scorrevole')
        verbose_name_plural = _('Banner Scorrevoli')        
        ordering  = ('type', 'order', 'title',)
        get_latest_by = 'name'    
        
    def __unicode__(self):
        return self.title

    def save(self, **kwargs): 
        self.slug = slugify(self.title)
        super(SlideBanner, self).save(**kwargs)
        
    def get_sites(self):
        sites = self.sites.all()
        return_list = [] 
        for site in sites:
            return_list.append(site.name)
        
        return return_list