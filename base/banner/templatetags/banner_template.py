
from django import template
from django.db.models import Q
from django.contrib.sites.models import Site

from banner.models import Banner, SlideBanner

register = template.Library()

def get_banners_batch(parser, token):
    tag_name, format_string, site = token.split_contents()    
    print format_string
    print site
    
    return LatestBannersNode(format_string, site)

class LatestBannersNode(template.Node):
    def __init__(self, format_string, site):
        self.format_string = format_string
        self.site = template.Variable(site)
         
    
    def render(self, context):
        site = self.site.resolve(context)
                
        q = Q()        
        if self.format_string:
            q = q & Q(type__id=int(self.format_string))
#
        if site:
            q = q & Q(sites=site)            
                
        context['banner_list'] = Banner.site_objects.filter(q)
        return ''

register.tag('get_banners', get_banners_batch)


##recupero solo un banner
def get_last_banner(parser, token):
    tag_name, format_string, site = token.split_contents()    
    print format_string
    print site
    
    return LastBannerNode(format_string, site)

class LastBannerNode(template.Node):
    def __init__(self, format_string, site):
        self.format_string = format_string
        self.site = site   
    
    def render(self, context):        
        q = Q(active=True)  
        if self.format_string:
            q = q & Q(type__id=int(self.format_string))
                    
        banner = None
        if Banner.site_objects.filter(q).count > 0:
            banner = Banner.site_objects.filter(q)[0]
        context['banner'] = banner
        return ''

register.tag('get_one_banner', get_last_banner)



##Banner Slider JCarousel.
def get_slide_banners_batch(parser, token):
    tag_name, format_string = token.split_contents()    
    print format_string
    
    return LatestSlideBannersNode(format_string)

class LatestSlideBannersNode(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):
        site = Site.objects.get_current()        
        q = Q(active=True) & Q(sites=site) 
        
        if self.format_string:
            q = q & Q(type__id=int(self.format_string))
                
        context['slide_banner_list'] = SlideBanner.site_objects.filter(q)
        return ''

register.tag('get_slide_banners', get_slide_banners_batch)
