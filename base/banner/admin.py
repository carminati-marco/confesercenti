from django.contrib import admin
from django import forms
from models import TypeBanner, Banner, SlideBanner

admin.site.register(TypeBanner)

class BannerAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'active', 'order', 'get_sites',)
    search_fields = ['title', 'description',]
    list_filter = ('sites', 'active', 'type', )

admin.site.register(Banner, BannerAdmin)

class SlideBannerAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'active', 'order', 'get_sites',)
    search_fields = ['title', 'description',]
    list_filter = ('sites', 'active', 'type', )


admin.site.register(SlideBanner, SlideBannerAdmin)
