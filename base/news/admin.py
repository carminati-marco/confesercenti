'''
Created on Apr 9, 2011

@author: suca
'''
from django.contrib import admin
from news.models import News, NewsType, Page, NewsPage
from news.forms import NewsAdminModelForm
from categories.models import SubCategory
from django.contrib.sites.models import Site

class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title','user', 'created_at', 'published')
    search_fields = ['body', 'title']
    ordering = ('-created_at',)
    list_filter = ('site', 'created_at', 'updated_at', 'published')
    filter_horizontal = ('category', 'macrocategory',)    
    raw_id_fields = ('user','documents',)
    
    fieldsets = [
        ('Generale', {'fields': ['title', 'slug'],}),
        ('', {'fields': ['body']}),
        ('Pubblicato', {'fields': ['published', 'pub_date', 'image', 'type']}),
        ('Sito', {'fields': ['site', 'macrocategory', 'category', 'documents']}),
        #('Pagina', {'fields': ['page', 'order',]})
    ]
    form = NewsAdminModelForm
    
    def save_model(self, request, obj, form, change):
        """ Autofill in author when blank on save models. """
        obj.user = request.user
        obj.save()
        
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "category":
            kwargs["queryset"] = SubCategory.objects.filter(sites = Site.objects.get_current())
        return super(NewsAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
    # EndDef


admin.site.register(News, NewsAdmin)
admin.site.register(NewsType)

class InlineNewsPageInstance(admin.TabularInline):
    model = NewsPage
    extra = 1
    fields = ('news', 'order',)
#    readonly_fields = ('title',)
     

class PageAdmin(admin.ModelAdmin):
    inlines = [InlineNewsPageInstance]

admin.site.register(Page, PageAdmin)

admin.site.register(NewsPage)