from django.conf.urls.defaults import *
from news.feeds import RssLatestNewsFeed, AtomLatestNewsFeed

from django.conf import settings
my_logger = settings.MY_LOGGER
my_logger.debug('inside news.urls')

feeds = {
    'rss': RssLatestNewsFeed,
    'atom': AtomLatestNewsFeed
}


urlpatterns = patterns('news.views',
    url(r'^$', 'listing', name='list-news'),
    url(r'^news_admin/$', 'news_admin', name='news_admin'),
    url(r'^(?P<page>\d+)/$', 'listing', name='list-news'),
    url(r'^(?P<slug>[-\w]+)/(?P<page_id>\d+)/$', 'news_details', name='news-details'),
    
    url(r'^search/', 'search', name = 'news_search'),
    
    url(r'^search_corsi/', 'search', { 'news_type' : 2 }, 'corsi_search'),
    url(r'^search_promozioni/', 'search', { 'news_type' : 3 }, 'promozioni_search'),
    url(r'^search_convenzioni/', 'search', { 'news_type' : 4 }, 'conv_search'),
        
)

urlpatterns += patterns('',
    # RSS
    (r'feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': feeds})
)
