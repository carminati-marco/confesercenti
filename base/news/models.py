# encoding: utf-8
from django.contrib.auth.models import User
from django.db import models
from news.managers import PublishedNewsManager, PubDateNewsManager
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site, SiteManager
from datetime import datetime
from categories.models import Category, SubCategory
from common.thumbs import ImageWithThumbsField

from documents.models import Document
from django.contrib.sites.managers import CurrentSiteManager

from django.conf import settings
my_logger = settings.MY_LOGGER
my_logger.debug('inside models news')

class NewsType(models.Model):
    name = models.CharField(max_length=255, verbose_name = _('Nome'))
    
    def __unicode__(self):
        return u"%s" % self.name
    
    class Meta:
        verbose_name = _("Tipo Notizia")
        verbose_name_plural = _("Tipi Notizie")
        
class Page(models.Model):
#    order = models.IntegerField(default = 0, verbose_name = _('Valore posizione'))
    name = models.CharField(max_length=255, verbose_name = _('Nome Posizione'))
    
    class Meta:
        #ordering = ['-order']
        verbose_name = _("Pagina")
        verbose_name_plural = _("Pagine")
        
    def __unicode__(self):
        return self.name
        
    def get_page_news(self):
        return News.on_site.filter(page = self).order_by('newspage__order')

class News(models.Model):
    #pubs_objects = PublishedNewsManager()
    objects = models.Manager()
    pubs_objects = PubDateNewsManager()
    on_site = CurrentSiteManager()
    
    # Optional
    title = models.CharField(max_length=255, verbose_name = _('Titolo')) 
    slug = models.SlugField(unique=True)
    body = models.TextField(verbose_name = _('Testo'))
    published = models.BooleanField(default=False, verbose_name = _('Pubblicato'))
    
    pub_date = models.DateTimeField(verbose_name = _('Data di pubblicazione'), default = datetime.now)
    site = models.ManyToManyField(Site, verbose_name = _('Sito'))    
    macrocategory = models.ManyToManyField(Category, verbose_name = _('Macro Categoria'), blank = True, null = True)
    category = models.ManyToManyField(SubCategory, verbose_name = _('Categoria'), blank = True, null = True)
    
    type = models.ForeignKey(NewsType, verbose_name = _('Tipo notizia'), blank = True, null = True)
    
    visits = models.IntegerField(default = 0, verbose_name = _('Numero visite'))
    
    page = models.ManyToManyField(Page, verbose_name = _('Collegamento Pagina'), blank = True, null = True, through = 'NewsPage')
    order = models.IntegerField(default = 0, verbose_name = _('Posizione Notizia'))
    
    # Automatic, verbose_name = _('Categoria')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)

    documents = models.ManyToManyField(Document, blank = True, null = True)
    
    image =  ImageWithThumbsField(upload_to='news/images', sizes=((125,125),(200,200),(500,220)),
                                  verbose_name = _('Immagine'))
    
    def __unicode__(self):
        return u"%s" % self.title
    
    def get_absolute_url(self):
        return "/news/%s/1/" % self.slug
    
    class Meta:
        ordering = ['-pub_date', '-order',]
        verbose_name = _("Notizia")
        verbose_name_plural = _("Notizie")


class NewsPage(models.Model):
    news = models.ForeignKey(News, verbose_name = _('Notizia'))
    page = models.ForeignKey(Page, verbose_name = _('Pagina'))
    created = models.DateTimeField(default = datetime.now, verbose_name = _("Data creazione"))
    order = models.IntegerField(verbose_name = _("Ordine"), default = 0)
    
    def __unicode__(self):
        return unicode(self.page) + unicode(' | ') + unicode(self.news)        
"""
from django.db import models

class Menu(models.Model):
    name = models.CharField(max_length = 100)
    
    def __unicode__(self):
        return self.name

class Item(models.Model):
    menu = models.ForeignKey(Menu)
    name = models.CharField(max_length = 100)
    url = models.CharField(max_length = 100)
    order = models.IntegerField(blank = True, null = True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ('order',)

# admin.py

from django.contrib import admin
from django import forms

from models import Menu, Item

class MenuForm(forms.ModelForm):
    model = Menu
    class Media:
        js = (
            '/static/js/jquery-latest.js',
            '/static/js/ui.base.js',
            '/static/js/ui.sortable.js',
            '/static/js/menu-sort.js',
        )

class ItemInline(admin.StackedInline):
    model = Item

admin.site.register(Menu,
    inlines = [ItemInline],
    form = MenuForm,
)

/* menu-sort.js */

jQuery(function($) {
    $('div.inline-group').sortable({
        /*containment: 'parent',
        zindex: 10, */
        items: 'div.inline-related',
        handle: 'h3:first',
        update: function() {
            $(this).find('div.inline-related').each(function(i) {
                if ($(this).find('input[id$=name]').val()) {
                    $(this).find('input[id$=order]').val(i+1);
                }
            });
        }
    });
    $('div.inline-related h3').css('cursor', 'move');
    $('div.inline-related').find('input[id$=order]').parent('div').hide();
});
"""
