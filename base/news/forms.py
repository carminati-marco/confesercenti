'''
Created on April 18, 2011

@author: suca
'''
from django import forms
from news.models import News, Page
from news.widgets import WYMEditor
from django.utils.translation import ugettext_lazy as _

from tinymce.widgets import TinyMCE

class NewsAdminModelForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label = _('Testo'))

    class Meta:
        model = News
        
class NewsAdmin(forms.Form):
    news = forms.ModelMultipleChoiceField(queryset = Page.objects.get(id = 1).get_page_news(), required = False)
    
    def save(self):
        print ('NewsAdmin save')
        ##recupera la pagina di questo sito
        page = Page.objects.get(id = 1)
        
        ##recupera le news attualmente in pagina e le toglie
        news_list = News.objects.filter(order__gte = 0, page = page)
        for new in news_list:
            new.page = None
            new.order = 0
            new.save()
        
        order = 50
        counter = 0
        
        for new in self.cleaned_data['news']:
            new.page = page
            new.order = order - counter
            new.save()
            
        return page