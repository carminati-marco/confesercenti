import datetime
from haystack.indexes import *
from haystack import site
from news.models import News


class NewsIndex(SearchIndex):
    text = CharField(document=True, use_template=True, model_attr = 'body')
    title = CharField(model_attr='title')
    author = CharField(model_attr='user')
    pub_date = DateTimeField(model_attr='pub_date')

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return News.objects.filter(pub_date__lte=datetime.datetime.now())


site.register(News, NewsIndex)
