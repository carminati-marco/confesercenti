from django.db import models
from datetime import datetime
from django.conf import settings

class PublishedNewsManager(models.Manager):
    def all(self):
        return self.filter()

    def filter(self, *args, **kwargs):
        return super(PublishedNewsManager, self).filter(published=True).filter(*args, **kwargs)
    
    
##added
class PubDateNewsManager(models.Manager):
    def get_query_set(self):
        return super(PubDateNewsManager, self).get_query_set().filter(published = True, pub_date__lte = datetime.now(),
                                                                      site__id = settings.SITE_ID)
