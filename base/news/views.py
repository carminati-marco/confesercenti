from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from news.models import News, Page
from news.forms import NewsAdmin
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from news.settings import NEWS_PER_PAGE, NUMBER_TRUNCATE_WORDS
from django.http import Http404

from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.http import HttpResponse, HttpResponseRedirect

from django.db.models import Q
from django.conf import settings

def listing(request, page=None):
    news_list = News.pubs_objects.all()
    paginator = Paginator(news_list, NEWS_PER_PAGE)
    
    # Make sure page request is an int. If not, deliver first page.
    if page is None:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        news = paginator.page(page)
    except (EmptyPage, InvalidPage):
        news = paginator.page(paginator.num_pages)

    return render_to_response('news/news_listing.html',
        {
         'news': news,
         'words': NUMBER_TRUNCATE_WORDS,
        },
        context_instance=RequestContext(request))
    
def news_details(request, slug=None, page_id=None):
    news = get_object_or_404(News, slug=slug)
    
    ##nel caso in cui visits sia null
    if news.visits == None:
        news.visits = 0
        
    news.visits = news.visits + 1
    news.save()
    
    if news.published != True:
        raise Http404
    
    # Make sure page request is an int. If not, deliver first page.
    try:
        page_id = int(request.GET.get('page', '1'))
    except ValueError:
        page_id = 1
    
    return render_to_response('news/news_detail.html',
        {
         'news': news,
         'page_id': page_id,
        },
        context_instance=RequestContext(request))
    
def home(request, template_name = 'home.html'):
    #news_list = News.pubs_objects.all()[0:4]
    page_news_id = settings.PAGE_NEWS_ID
    news_list = Page.objects.get(id = page_news_id).get_page_news()
    query = news_list.query
    
    site = Site.objects.get_current()
#    if site.id == 3:
#        template_name = 'base_wip.html'
    
    return render_to_response(template_name,
        {
         'page' : Page.objects.get(id = page_news_id),
         'news_list': news_list,
         'query' : query,
        },
        context_instance=RequestContext(request))
    
def home_develop(request, template_name = 'home.html'):
    #news_list = News.pubs_objects.all()[0:4]
    page_news_id = settings.PAGE_NEWS_ID
    news_list = Page.objects.get(id = page_news_id).get_page_news()
    query = news_list.query
    
    
    return render_to_response(template_name,
        {
         'page' : Page.objects.get(id = page_news_id),
         'news_list': news_list,
         'query' : query,
        },
        context_instance=RequestContext(request))    
    
    
def redirect_cescot(request):
    print 'di qui'
    return HttpResponseRedirect('/chisiamo/cescot')
        
    
def search(request, news_type = None):
    news_list = None
    print 'search'
    print news_type
    
    
    word = request.GET.get('search_text', '')
    
    if word in ('corso', 'corsi'):
        result = 'corso'
        news_list = News.pubs_objects.filter(type = int(settings.NEWS_COURSES_TYPE_ID))
        
    elif word in ('promozioni', 'promozione'):
        result = 'promo'
        news_list = News.pubs_objects.filter(type = int(settings.NEWS_PROMO_TYPE_ID))
    
    else:
        ##cerca
        result = 'cerca'
        q = Q()
        if news_type:
            q = Q(type = news_type)
            
            print news_type    
        
        
        if word:
            q = q & (Q(title__search = word) | Q(body__search = word))
        
        print q
        news_list = News.pubs_objects.filter(q)

    
    
    return render_to_response('news/news_listing_search.html',
        {
         'word' : word,
         'most_viewed_news_category': news_list,
         'result' : result,
        },
        context_instance=RequestContext(request))

@login_required
def news_admin(request, form_class = NewsAdmin):
    
    print ('news_admin')
    
    if not request.user.is_staff:
        raise Http404
    
    news_list = News.objects.all().order_by('-created_at')[0:25]
    page = Page.objects.get(id = 1)
    
    
    if request.method == "POST":
        print ('news_admin POST')
        news_form = NewsAdmin(request.POST)
        
        if news_form.is_valid():
            page = news_form.save()
        else:
            print ('news_admin.errors : ' + unicode(news_form.errors))
    else:
        print ('news_admin GET : ')
        news_form = NewsAdmin()
    
    return render_to_response('news/news_admin.html',
        {
         'news_list': news_list,
         'page' : page,
         'news_form' : news_form,
        },
        context_instance=RequestContext(request))
    
    
    
    
    
    