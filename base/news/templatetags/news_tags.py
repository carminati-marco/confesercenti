from django import template
from news.models import News
from courses.models import Course
from conventions.models import Convention
from announcement.models import Announcement

from django.conf import settings

register = template.Library()

my_logger = settings.MY_LOGGER
my_logger.debug('inside news_tags')

def get_most_viewed_news(parser, token):
    format_string = ''
    return MostViewedNews(format_string)

class MostViewedNews(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):                
        context['most_viewed_news'] = News.pubs_objects.all().order_by('-visits')[0:settings.NEWS_PER_TAG]
        return ''

register.tag('get_most_viewed_news', get_most_viewed_news)

def get_latest_courses(parser, token):
    format_string = ''
    return LatestCourses(format_string)

class LatestCourses(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):                
        #context['latest_courses'] = News.pubs_objects.filter(type = int(settings.NEWS_COURSES_TYPE_ID))[0:5]
        context['latest_courses'] = Course.pubs_objects.all()[0:settings.NEWS_PER_TAG]
        return ''

register.tag('get_latest_courses', get_latest_courses)

def get_latest_promo(parser, token):
    format_string = ''
    return LatestPromo(format_string)

class LatestPromo(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):                
        context['latest_promo'] = News.pubs_objects.filter(type = int(settings.NEWS_PROMO_TYPE_ID))[0:settings.NEWS_PER_TAG]
        return ''

register.tag('get_latest_promo', get_latest_promo)

def get_latest_conv(parser, token):
    format_string = ''
    return LatestConv(format_string)

class LatestConv(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string   
    
    def render(self, context):
        context['latest_conv'] = Convention.pubs_objects.filter()[0:settings.NEWS_PER_TAG]
        return ''

register.tag('get_latest_conv', get_latest_conv)


def get_latest_anno(parser, token):
    format_string = ''
    return LatestConvInno(format_string)

class LatestConvInno(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string

    def render(self, context):
        context['latest_anno'] = Announcement.on_site.filter()[0:settings.NEWS_PER_TAG]
        return ''

register.tag('get_latest_anno', get_latest_anno)

def get_most_viewed_news_category(category, type, order_by = None):
    subcategory_list = []
    type = int(type)
    
    for subcategory in category.get_subcategories():
        subcategory_list.append(subcategory)
    
    if type == settings.NEWS_NEWS_TYPE_ID or type == settings.NEWS_PROMO_TYPE_ID:
        most_viewed_news_category = News.pubs_objects.filter(category__in = subcategory_list, type = type).distinct()
    if type == settings.NEWS_COURSES_TYPE_ID:
        most_viewed_news_category = Course.pubs_objects.filter(category__in = subcategory_list, type = type).distinct()
    if type == settings.NEWS_CONV_TYPE_ID:
        most_viewed_news_category = Convention.pubs_objects.filter(category__in = subcategory_list, type = type).distinct()
    if type == settings.NEWS_ANNOUNCEMENT_TYPE_ID:
        most_viewed_news_category = Announcement.pubs_objects.filter(category__in = subcategory_list, type = type)

    if order_by:
        most_viewed_news_category = most_viewed_news_category.order_by(order_by)

    most_viewed_news_category = most_viewed_news_category[0:settings.NEWS_PER_TAG]

#    most_viewed_news_category = News.pubs_objects.filter(category__in = subcategory_list, type = type).order_by('-visits')[0:5]

    return {'most_viewed_news_category': most_viewed_news_category}

register.inclusion_tag('news/most_viewed_news.html')(get_most_viewed_news_category)

def get_most_viewed_news_category_lateral(category, type, order_by = None):
    return get_most_viewed_news_category(category, type, order_by = order_by)

register.inclusion_tag('news/most_viewed_news_lateral.html')(get_most_viewed_news_category_lateral)


def get_most_viewed_news_subcategory(subcategory, type, order_by = None):
    type = int(type)
    ##alain marco tun mettere filtro per category!!!!!
    if type == settings.NEWS_NEWS_TYPE_ID or type == settings.NEWS_PROMO_TYPE_ID:
        most_viewed_news_category = News.pubs_objects.filter(category = subcategory, type = type)
    if type == settings.NEWS_COURSES_TYPE_ID:
        most_viewed_news_category = Course.pubs_objects.filter(category = subcategory)
    if type == settings.NEWS_CONV_TYPE_ID:
        most_viewed_news_category = Convention.pubs_objects.filter(category = subcategory)
    if type == settings.NEWS_ANNOUNCEMENT_TYPE_ID:
        most_viewed_news_category = Announcement.pubs_objects.filter(category = subcategory)

    if order_by:
        most_viewed_news_category = most_viewed_news_category.order_by(order_by)
        
    most_viewed_news_category = most_viewed_news_category[0:settings.NEWS_PER_TAG]
    
#    most_viewed_news_category = News.pubs_objects.filter(category = subcategory, type = type).order_by('-visits')[0:5]
    return {'most_viewed_news_category': most_viewed_news_category}

register.inclusion_tag('news/most_viewed_news.html')(get_most_viewed_news_subcategory)

def get_most_viewed_news_subcategory_lateral(subcategory, type, order_by = None):
    return get_most_viewed_news_subcategory(subcategory, type, order_by = order_by)

register.inclusion_tag('news/most_viewed_news_lateral.html')(get_most_viewed_news_subcategory_lateral)