<!-- #include file = "include/cnn_open.asp" -->
<%
If session("amministratore")<>"GPSimpresa" then response.redirect "../login.asp?session=false"

'******** totale aziende
Set rs_pag = Server.CreateObject("ADODB.Recordset")
rs_pag.Open	"SELECT COUNT(*) AS RecordCount FROM aziende", cnn, 1
if not rs_pag.eof then			
	n_aziende = cInt(rs_pag("RecordCount"))
end if
rs_pag.Close
Set rs_pag = Nothing
'******** totale uscite
Set rs_pag = Server.CreateObject("ADODB.Recordset")
rs_pag.Open	"SELECT COUNT(*) AS RecordCount FROM uscite", cnn, 1
if not rs_pag.eof then			
	n_uscite = cInt(rs_pag("RecordCount"))
end if
rs_pag.Close
Set rs_pag = Nothing
'******** totale report
Set rs_pag = Server.CreateObject("ADODB.Recordset")
rs_pag.Open	"SELECT COUNT(*) AS RecordCount FROM report", cnn, 1
if not rs_pag.eof then			
	n_report = cInt(rs_pag("RecordCount"))
end if
rs_pag.Close
Set rs_pag = Nothing
'******** totale allegati
Set rs_pag = Server.CreateObject("ADODB.Recordset")
rs_pag.Open	"SELECT COUNT(*) AS RecordCount FROM allegati", cnn, 1
if not rs_pag.eof then			
	n_allegati = cInt(rs_pag("RecordCount"))
end if
rs_pag.Close
Set rs_pag = Nothing
'******** totale aconsuntivi
Set rs_pag = Server.CreateObject("ADODB.Recordset")
rs_pag.Open	"SELECT COUNT(*) AS RecordCount FROM consuntivi", cnn, 1
if not rs_pag.eof then			
	n_consuntivi = cInt(rs_pag("RecordCount"))
end if
rs_pag.Close
Set rs_pag = Nothing
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> GPSimpresa - il Navigatore per l'Innovazione </title>
<link rel="stylesheet" href="res/stile.css" type="text/css">
<script language="JavaScript" src="res/menu.js"></script>
</head>

<body>
<!-- #include file = "include/menu.asp" -->
<table cellpadding="0" cellspacing="0" width="990" align="center" border="0">
  <tr>
    <td height="500" valign="top"><b>Benvenuto nell'area riservata di GPSimpresa !</b><br><br>
	
	Ad oggi il servizio offerto da Confesercenti Bergamo ha raccolto <b><%=n_aziende%> aziende</b> con rispettivamento <b><%=n_uscite%> attivit�</b> effettuate.<br><br>
	
	Di queste <b><%=n_aziende%> aziende</b> <b><%=n_report%> hanno il report</b> eseguito.<br><br>
	
	Vi sono anche <b><%=n_allegati%> allegati</b>.<br><br>
	
	<b><%=n_consuntivi%> consuntivi</b></td>
  </tr>
</table>

</body>
</html>
<!-- #include file = "include/cnn_close.asp" -->