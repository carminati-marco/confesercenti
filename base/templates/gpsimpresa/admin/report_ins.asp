<!-- #include file = "include/cnn_open.asp" -->
<%
If session("amministratore")<>"GPSimpresa" then response.redirect "../login.asp?session=false"

gg_re							=Day(Now)
mm_re							=Month(Now)
yyyy_re							=Year(Now)
hh_re							=Hour(Now)
min_re							=Minute(Now)

id_qu				=request.querystring("id")
If id_qu<>"" then
Set rs_report = Server.CreateObject("ADODB.RecordSet") 
	rs_report.Open "SELECT * FROM report WHERE id_azienda='"&id_qu&"'" ,cnn,1
	If Not rs_report.eof Then
	id_re						=rs_report("id")
	data_re						=rs_report("data")
	ora_re						=rs_report("ora")
	dove_re						=rs_report("dove")
	consulente_re				=rs_report("consulente")
	flussi_operativi_re			=rs_report("flussi_operativi")
	n_clienti_re				=rs_report("n_clienti")
	n_fornitori_re				=rs_report("n_fornitori")
	n_articoli_re				=rs_report("n_articoli")
	ddt_re						=rs_report("ddt")
	fatt_attive_re				=rs_report("fatt_attive")
	fatt_passive_re				=rs_report("fatt_passive")
	n_cari_scari_re				=rs_report("n_cari_scari")
	n_vendite_re				=rs_report("n_vendite")
	movim_banca_re				=rs_report("movim_banca")
	ad_gestionale_re			=rs_report("ad_gestionale")
	magazzino_re				=rs_report("magazzino")
	ven_prod_re					=rs_report("ven_prod")
	acquisti_re					=rs_report("acquisti")
	marketing_re				=rs_report("marketing")
	uff_tecnico_re				=rs_report("uff_tecnico")
	org_informatica_re			=rs_report("org_informatica")
	ambiente_so_re				=rs_report("ambiente_so")
	db_re						=rs_report("db")
	rete_re						=rs_report("rete")
	office_automation_re		=rs_report("office_automation")
	sito_internet_re			=rs_report("sito_internet")
	accesso_internet_re			=rs_report("accesso_internet")
	gestionale_re				=rs_report("gestionale")
	verticali_personaliz_re		=rs_report("verticali_personaliz")
	altro_software_re			=rs_report("altro_software")
	pc_amm_re					=rs_report("pc_amm")
	pc_matk_re					=rs_report("pc_matk")
	pc_prod_re					=rs_report("pc_prod")
	pc_vend_re					=rs_report("pc_vend")
	pc_mesi_anz_re				=rs_report("pc_mesi_anz")
	obiettivi_re				=rs_report("obiettivi")
	ambito_re					=rs_report("ambito")
	funzioni_re					=rs_report("funzioni")
	vincoli_re					=rs_report("vincoli")
	risorse_persone_re			=rs_report("risorse_persone")
	assunzioni_re				=rs_report("assunzioni")
	timing_re					=rs_report("timing")
	gg_re						=Day(data_re)
	mm_re						=Month(data_re)
	yyyy_re						=Year(data_re)
	hh_re						=Hour(ora_re)
	min_re						=minute(ora_re)
	End if
rs_report.close
Set rs_report = Nothing
End if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> GPSimpresa - il Navigatore per l'Innovazione </title>
<link rel="stylesheet" href="res/stile.css" type="text/css">
<script language="JavaScript" src="res/menu.js"></script>
</head>

<body>
<!-- #include file = "include/menu.asp" -->
<% If id_re="" Then %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>INSERISCI REPORT :</h1></td>
  </tr>
</table>
<% else %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>MODIFICA REPORT :</h1></td>
  </tr>
</table>
<% End if %>
<form name="report" method="post" action="report_save.asp">
<input type="hidden" name="id" value="<%=id_re%>">
<input type="hidden" name="id_azienda" value="<%=id_qu%>">
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td valign="top">
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="634"><b>Data</b>:&nbsp;</td>
			<td width="120"><input type="text" name="gg" style="width:20px;" class="inse" value="<%=gg_re%>" maxlength="2"> / <input type="text" name="mm" style="width:20px;" class="inse" value="<%=mm_re%>" maxlength="2"> / <input type="text" name="yyyy" style="width:35px;" class="inse" value="<%=yyyy_re%>" maxlength="4"></td>
			<td class="righe" width="171"><b>Ora</b>:&nbsp;</td>
			<td width="65"><input type="text" name="hh" style="width:20px;" class="inse" value="<%=hh_re%>" maxlength="2"> : <input type="text" name="min" style="width:20px;" class="inse" value="<%=min_re%>" maxlength="2"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="155"><b>Dove</b>:&nbsp;</td>
			<td width="340"><input type="text" name="dove" style="width:340px;" class="inse" maxlength="50" value="<%=dove_re%>"></td>
			<td class="righe" width="150"><b>Consulente</b>:&nbsp;</td>
			<td width="350"><input type="text" name="consulente" style="width:350px;" class="inse" maxlength="50" value="<%=consulente_re%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;">
		  <tr>
		    <td height="30"><b>Intervista :</b></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Flussi operativi</b>:</td>
			<td width="840"><textarea name="flussi_operativi" style="width:840px;height:100px;" class="inse_textarea"><%=flussi_operativi_re%></textarea></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="145"><b>Volumi</b>:&nbsp;</td>
			<td class="righe" width="100"><b>N&deg; Clienti</b>:&nbsp;</td>
			<td width="130"><input type="text" name="n_clienti" style="width:80px;" class="inse" maxlength="50" value="<%=n_clienti_re%>"></td>
			<td class="righe" width="100"><b>N&deg; Fornitori</b>:&nbsp;</td>
			<td width="130"><input type="text" name="n_fornitori" style="width:80px;" class="inse" maxlength="50" value="<%=n_fornitori_re%>"></td>
			<td class="righe" width="100"><b>N&deg; Articoli</b>:&nbsp;</td>
			<td width="130"><input type="text" name="n_articoli" style="width:80px;" class="inse" maxlength="50" value="<%=n_articoli_re%>"></td>
			<td class="righe" width="150"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="145"><b>Movimenti mese</b>:&nbsp;</td>
			<td class="righe" width="100"><b>Ddt</b>:&nbsp;</td>
			<td width="130"><input type="text" name="ddt" style="width:80px;" class="inse" maxlength="50" value="<%=ddt_re%>"></td>
			<td class="righe" width="100"><b>Fatt. Attive</b>:&nbsp;</td>
			<td width="130"><input type="text" name="fatt_attive" style="width:80px;" class="inse" maxlength="50" value="<%=fatt_attive_re%>"></td>
			<td class="righe" width="100"><b>Fatt. Passive</b>:&nbsp;</td>
			<td width="130"><input type="text" name="fatt_passive" style="width:80px;" class="inse" maxlength="50" value="<%=fatt_passive_re%>"></td>
			<td class="righe" width="150"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="145"></td>
			<td class="righe" width="100"><b>N&deg; Cari/Scari</b>:&nbsp;</td>
			<td width="130"><input type="text" name="n_cari_scari" style="width:80px;" class="inse" maxlength="50" value="<%=n_cari_scari_re%>"></td>
			<td class="righe" width="100"><b>N&deg; Vendite</b>:&nbsp;</td>
			<td width="130"><input type="text" name="n_vendite" style="width:80px;" class="inse" maxlength="50" value="<%=n_vendite_re%>"></td>
			<td class="righe" width="100"><b>Movim. Banca</b>:&nbsp;</td>
			<td width="130"><input type="text" name="movim_banca" style="width:80px;" class="inse" maxlength="50" value="<%=movim_banca_re%>"></td>
			<td class="righe" width="150"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="145"><b>Addetti</b>:&nbsp;</td>
			<td class="righe" width="100"><b>Gestionale</b>:&nbsp;</td>
			<td width="130"><input type="text" name="ad_gestionale" style="width:80px;" class="inse" maxlength="50" value="<%=ad_gestionale_re%>"></td>
			<td class="righe" width="100"><b>Magazzino</b>:&nbsp;</td>
			<td width="130"><input type="text" name="magazzino" style="width:80px;" class="inse" maxlength="50" value="<%=magazzino_re%>"></td>
			<td class="righe" width="100"><b>Ven/Prod</b>:&nbsp;</td>
			<td width="130"><input type="text" name="ven_prod" style="width:80px;" class="inse" maxlength="50" value="<%=ven_prod_re%>"></td>
			<td class="righe" width="150"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="145"></td>
			<td class="righe" width="100"><b>Acquisti</b>:&nbsp;</td>
			<td width="130"><input type="text" name="acquisti" style="width:80px;" class="inse" maxlength="50" value="<%=acquisti_re%>"></td>
			<td class="righe" width="100"><b>Marketing</b>:&nbsp;</td>
			<td width="130"><input type="text" name="marketing" style="width:80px;" class="inse" maxlength="50" value="<%=marketing_re%>"></td>
			<td class="righe" width="100"><b>Uff. Tecnico</b>:&nbsp;</td>
			<td width="130"><input type="text" name="uff_tecnico" style="width:80px;" class="inse" maxlength="50" value="<%=uff_tecnico_re%>"></td>
			<td class="righe" width="150"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" bgcolor="#eeeeee">
		  <tr>
		    <td class="righe" width="150"><b>Org. informatica</b>:&nbsp;</td>
			<td width="840"><input type="text" name="org_informatica" style="width:840px;" class="inse" maxlength="250" value="<%=org_informatica_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Ambiente e SO</b>:&nbsp;</td>
			<td width="840"><input type="text" name="ambiente_so" style="width:840px;" class="inse" maxlength="250" value="<%=ambiente_so_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>DB</b>:&nbsp;</td>
			<td width="840"><input type="text" name="db" style="width:840px;" class="inse" maxlength="250" value="<%=db_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Rete</b>:&nbsp;</td>
			<td width="840"><input type="text" name="rete" style="width:840px;" class="inse" maxlength="250" value="<%=rete_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Office automation</b>:&nbsp;</td>
			<td width="840"><input type="text" name="office_automation" style="width:840px;" class="inse" maxlength="250" value="<%=office_automation_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Sito internet</b>:&nbsp;</td>
			<td width="840"><input type="text" name="sito_internet" style="width:840px;" class="inse" maxlength="250" value="<%=sito_internet_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Accesso internet</b>:&nbsp;</td>
			<td width="840"><input type="text" name="accesso_internet" style="width:840px;" class="inse" maxlength="250" value="<%=accesso_internet_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Gestionale</b>:&nbsp;</td>
			<td width="840"><input type="text" name="gestionale" style="width:840px;" class="inse" maxlength="250" value="<%=gestionale_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Verticali/Personaliz.</b>:&nbsp;</td>
			<td width="840"><input type="text" name="verticali_personaliz" style="width:840px;" class="inse" maxlength="250" value="<%=verticali_personaliz_re%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Altro software</b>:&nbsp;</td>
			<td width="840"><input type="text" name="altro_software" style="width:840px;" class="inse" maxlength="250" value="<%=altro_software_re%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" bgcolor="#eeeeee">
		  <tr>
		    <td class="righe" width="145"><b>Hardware</b>:&nbsp;</td>
			<td class="righe" width="150"><b>N&deg; PC Amm/Matk</b>:&nbsp;</td>
			<td width="100"><input type="text" name="pc_amm" style="width:40px;" class="inse" maxlength="50" value="<%=pc_amm_re%>"> <input type="text" name="pc_matk" style="width:40px;" class="inse" maxlength="50" value="<%=pc_matk_re%>"></td>
			<td class="righe" width="150"><b>N&deg; PC Prod/Vend</b>:&nbsp;</td>
			<td width="100"><input type="text" name="pc_prod" style="width:40px;" class="inse" maxlength="50" value="<%=pc_prod_re%>"> <input type="text" name="pc_vend" style="width:40px;" class="inse" maxlength="50" value="<%=pc_vend_re%>"></td>
			<td class="righe" width="150"><b>Mesi anz. media</b>:&nbsp;</td>
			<td width="100"><input type="text" name="pc_mesi_anz" style="width:80px;" class="inse" maxlength="50" value="<%=pc_mesi_anz_re%>"></td>
			<td class="righe" width="90"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;">
		  <tr>
		    <td height="30"><b>Aspettative di sviluppo :</b></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Obiettivi</b>:</td>
			<td width="840"><textarea name="obiettivi" style="width:840px;height:50px;" class="inse_textarea"><%=obiettivi_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Ambito</b>:</td>
			<td width="840"><textarea name="ambito" style="width:840px;height:50px;" class="inse_textarea"><%=ambito_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Funzioni</b>:</td>
			<td width="840"><textarea name="funzioni" style="width:840px;height:50px;" class="inse_textarea"><%=funzioni_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Vincoli</b>:</td>
			<td width="840"><textarea name="vincoli" style="width:840px;height:50px;" class="inse_textarea"><%=vincoli_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Risorse persone</b>:</td>
			<td width="840"><textarea name="risorse_persone" style="width:840px;height:50px;" class="inse_textarea"><%=risorse_persone_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Assunzioni</b>:</td>
			<td width="840"><textarea name="assunzioni" style="width:840px;height:50px;" class="inse_textarea"><%=assunzioni_re%></textarea></td>
		  </tr>
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Timing</b>:</td>
			<td width="840"><textarea name="timing" style="width:840px;height:50px;" class="inse_textarea"><%=timing_re%></textarea></td>
		  </tr>
		</table>
		<% If id_re="" Then %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_003.gif" border="0"></td>
		  </tr>
		</table>
		<% else %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_004.gif" border="0"></td>
		  </tr>
		</table>
		<% End if %>
	</td>
  </tr>
</table>
</form>
	

</body>
</html>
<!-- #include file = "include/cnn_close.asp" -->