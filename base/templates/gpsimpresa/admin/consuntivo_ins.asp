<!-- #include file = "include/cnn_open.asp" -->
<%
If session("amministratore")<>"GPSimpresa" then response.redirect "../login.asp?session=false"

id_qu				=request.querystring("id")
If id_qu<>"" then
Set rs_consuntivi = Server.CreateObject("ADODB.RecordSet") 
	rs_consuntivi.Open "SELECT * FROM consuntivi WHERE id_azienda='"&id_qu&"'" ,cnn,1
	If Not rs_consuntivi.eof Then
	id_co						=rs_consuntivi("id")
	accettazione_incontri_co	=rs_consuntivi("accettazione_incontri")
	accettazione_ore_co			=rs_consuntivi("accettazione_ore")
	analisi_incontri_co			=rs_consuntivi("analisi_incontri")
	analisi_ore_co				=rs_consuntivi("analisi_ore")
	progetto_incontri_co		=rs_consuntivi("progetto_incontri")
	progetto_ore_co				=rs_consuntivi("progetto_ore")
	note_co						=rs_consuntivi("note")
	End if
rs_consuntivi.close
Set rs_consuntivi = Nothing
End if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> GPSimpresa - il Navigatore per l'Innovazione </title>
<link rel="stylesheet" href="res/stile.css" type="text/css">
<script language="JavaScript" src="res/menu.js"></script>
</head>

<body>
<!-- #include file = "include/menu.asp" -->
<% If id_co="" Then %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>INSERISCI CONSUNTIVO :</h1></td>
  </tr>
</table>
<% else %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>MODIFICA CONSUNTIVO :</h1></td>
  </tr>
</table>
<% End if %>
<form name="consuntivo" method="post" action="consuntivo_save.asp">
<input type="hidden" name="id" value="<%=id_co%>">
<input type="hidden" name="id_azienda" value="<%=id_qu%>">
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td valign="top">
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="345"><b>Accettazione</b> (Consulente Junior):&nbsp;</td>
			<td class="righe" width="100"><b>N&deg; Incontri</b>:&nbsp;</td>
			<td width="130"><input type="text" name="accettazione_incontri" style="width:80px;" class="inse" maxlength="50" value="<%=accettazione_incontri_co%>"></td>
			<td class="righe" width="130"><b>Ore dedicate</b>:&nbsp;</td>
			<td width="130"><input type="text" name="accettazione_ore" style="width:80px;" class="inse" maxlength="50" value="<%=accettazione_ore_co%>"></td>
			<td class="righe" width="155"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="345"><b>Analisi</b> (Consulente Senior):&nbsp;</td>
			<td class="righe" width="100"><b>N&deg; Incontri</b>:&nbsp;</td>
			<td width="130"><input type="text" name="analisi_incontri" style="width:80px;" class="inse" maxlength="50" value="<%=analisi_incontri_co%>"></td>
			<td class="righe" width="130"><b>Ore dedicate</b>:&nbsp;</td>
			<td width="130"><input type="text" name="analisi_ore" style="width:80px;" class="inse" maxlength="50" value="<%=analisi_ore_co%>"></td>
			<td class="righe" width="155"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="345"><b>Progetto</b> (Consulente Senior):&nbsp;</td>
			<td class="righe" width="100"><b>N&deg; Incontri</b>:&nbsp;</td>
			<td width="130"><input type="text" name="progetto_incontri" style="width:80px;" class="inse" maxlength="50" value="<%=progetto_incontri_co%>"></td>
			<td class="righe" width="130"><b>Ore dedicate</b>:&nbsp;</td>
			<td width="130"><input type="text" name="progetto_ore" style="width:80px;" class="inse" maxlength="50" value="<%=progetto_ore_co%>"></td>
			<td class="righe" width="155"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Note</b>:</td>
			<td width="840"><textarea name="note" style="width:840px;height:100px;" class="inse_textarea"><%=note_co%></textarea></td>
		  </tr>
		</table>
		<% If id_co="" Then %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_009.gif" border="0"></td>
		  </tr>
		</table>
		<% else %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_010.gif" border="0"></td>
		  </tr>
		</table>
		<% End if %>
	</td>
  </tr>
</table>
</form>
	

</body>
</html>
<!-- #include file = "include/cnn_close.asp" -->