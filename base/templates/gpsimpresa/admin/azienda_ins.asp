<!-- #include file = "include/cnn_open.asp" -->
<%
If session("amministratore")<>"GPSimpresa" then response.redirect "../login.asp?session=false"

gg_az							=Day(Now)
mm_az							=Month(Now)
yyyy_az							=Year(Now)

id_qu				=request.querystring("id")
If id_qu<>"" then
Set rs_azienda = Server.CreateObject("ADODB.RecordSet") 
	rs_azienda.Open "SELECT * FROM aziende WHERE id='"&id_qu&"'" ,cnn,1
	If Not rs_azienda.eof Then
	id_az							=rs_azienda("id")
	data_az							=rs_azienda("data")
	sorgente_az						=rs_azienda("sorgente")
	ragione_sociale_az				=rs_azienda("ragione_sociale")
	indirizzo_az					=rs_azienda("indirizzo")
	cap_az							=rs_azienda("cap")
	comune_az						=rs_azienda("comune")
	riferimento_sig_az				=rs_azienda("riferimento_sig")
	ruolo_az						=rs_azienda("ruolo")
	telefono_az						=rs_azienda("telefono")
	cellulare_az					=rs_azienda("cellulare")
	fax_az							=rs_azienda("fax")
	email_az						=rs_azienda("email")
	settore_az						=rs_azienda("settore")
	attivita_az						=rs_azienda("attivita")
	n_addetti_az					=rs_azienda("n_addetti")
	dipendenti_tp_az				=rs_azienda("dipendenti_tp")
	dipendenti_pt_az				=rs_azienda("dipendenti_pt")
	fatturato_az					=rs_azienda("fatturato")
	interesse_gps_az				=rs_azienda("interesse_gps")
	attese_gps_az					=rs_azienda("attese_gps")
	gg_az							=Day(data_az)
	mm_az							=Month(data_az)
	yyyy_az							=Year(data_az)
	End if
rs_azienda.close
Set rs_azienda = Nothing
End if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> GPSimpresa - il Navigatore per l'Innovazione </title>
<link rel="stylesheet" href="res/stile.css" type="text/css">
<script language="JavaScript" src="res/menu.js"></script>
<script language="JavaScript">
function control(form){

	if (form.ragione_sociale.value.length < 1){
			alert('ATTENZIONE! ragione sociale non inserita.')
			form.ragione_sociale.className = "INVALIDINPUT";
			form.ragione_sociale.focus();
			return false;}
	if (form.indirizzo.value.length < 1){
			alert('ATTENZIONE! indirizzo - via non inserite.')
			form.indirizzo.className = "INVALIDINPUT";
			form.indirizzo.focus();
			return false;}
}	
</script>
</head>

<body>
<!-- #include file = "include/menu.asp" -->
<% If id_az="" Then %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>INSERISCI UNA NUOVA AZIENDA :</h1></td>
  </tr>
</table>
<% else %>
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td height="25" valign="top"><h1>MODIFICA AZIENDA :</h1></td>
  </tr>
</table>
<% End if %>
<form name="azienda" method="post" action="azienda_save.asp" onsubmit="return control(this)">
<table cellpadding="0" cellspacing="0" width="990" border="0" align="center">
  <tr>
    <td valign="top">
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="570"><b>Progetto N&deg;</b>:&nbsp;</td>
			<td width="100"><input type="text" name="id" style="width:80px;" class="inse" value="<%=id_az%>" readonly></td>
			<td class="righe" width="200"><b>Data primo contatto</b>:&nbsp;</td>
			<td width="120"><input type="text" name="gg" style="width:20px;" class="inse" value="<%=gg_az%>" maxlength="2"> / <input type="text" name="mm" style="width:20px;" class="inse" value="<%=mm_az%>" maxlength="2"> / <input type="text" name="yyyy" style="width:35px;" class="inse" value="<%=yyyy_az%>" maxlength="4"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-bottom:10px;">
		  <tr>
		    <td class="righe" width="150"><b>Sorgente</b>:&nbsp;</td>
			<td width="10"><input type="radio" name="sorgente" value="radio"<% If sorgente_az="radio" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Radio</td>
			<td width="10"><input type="radio" name="sorgente" value="tv"<% If sorgente_az="tv" Then response.write " checked"%>></td>
			<td width="100">&nbsp;TV</td>
			<td width="10"><input type="radio" name="sorgente" value="email"<% If sorgente_az="email" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Email</td>
			<td width="10"><input type="radio" name="sorgente" value="passaparola"<% If sorgente_az="passaparola" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Passaparola</td>
			<td width="10"><input type="radio" name="sorgente" value="altro"<% If sorgente_az="altro" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Altro</td>
			<td class="righe" width="290"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" bgcolor="#eeeeee">
		  <tr>
		    <td class="righe" width="150">* <b>Ragione Sociale</b>:&nbsp;</td>
			<td width="840"><input type="text" name="ragione_sociale" style="width:840px;" class="inse" maxlength="250" value="<%=ragione_sociale_az%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150">* <b>Indirizzo - Via</b>:&nbsp;</td>
			<td width="840"><input type="text" name="indirizzo" style="width:840px;" class="inse" maxlength="250" value="<%=indirizzo_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" bgcolor="#eeeeee">
		  <tr>
		    <td class="righe" width="150"><b>Cap</b>:&nbsp;</td>
			<td width="150"><input type="text" name="cap" style="width:50px;" class="inse" maxlength="5" value="<%=cap_az%>"></td>
			<td class="righe" width="150"><b>Comune</b>:&nbsp;</td>
			<td width="550"><input type="text" name="comune" style="width:550px;" class="inse" maxlength="250" value="<%=comune_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;">
		  <tr>
		    <td class="righe" width="150"><b>Riferimento Sig.</b>:&nbsp;</td>
			<td width="840"><input type="text" name="riferimento_sig" style="width:840px;" class="inse" maxlength="250" value="<%=riferimento_sig_az%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Ruolo</b>:&nbsp;</td>
			<td width="840"><input type="text" name="ruolo" style="width:840px;" class="inse" maxlength="250" value="<%=ruolo_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>Telefono</b>:&nbsp;</td>
			<td width="340"><input type="text" name="telefono" style="width:340px;" class="inse" maxlength="50" value="<%=telefono_az%>"></td>
			<td class="righe" width="150"><b>Cellulare</b>:&nbsp;</td>
			<td width="350"><input type="text" name="cellulare" style="width:350px;" class="inse" maxlength="50" value="<%=cellulare_az%>"></td>
		  </tr>
		  <tr>
		    <td class="righe" width="150"><b>Fax</b>:&nbsp;</td>
			<td width="340"><input type="text" name="fax" style="width:340px;" class="inse" maxlength="50" value="<%=fax_az%>"></td>
			<td class="righe" width="150"><b>Email</b>:&nbsp;</td>
			<td width="350"><input type="text" name="email" style="width:350px;" class="inse" maxlength="250" value="<%=email_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>Settore</b>:&nbsp;</td>
			<td width="10"><input type="radio" name="settore" value="commerciale"<% If settore_az="commerciale" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Commerciale</td>
			<td width="10"><input type="radio" name="settore" value="ristorativo"<% If settore_az="ristorativo" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Ristorativo</td>
			<td width="10"><input type="radio" name="settore" value="industriale"<% If settore_az="industriale" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Industriale</td>
			<td width="10"><input type="radio" name="settore" value="agricolo"<% If settore_az="agricolo" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Agricolo</td>
			<td width="10"><input type="radio" name="settore" value="turismo"<% If settore_az="turismo" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Turismo</td>
			<td width="10"><input type="radio" name="settore" value="servizi"<% If settore_az="servizi" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Servizi</td>
			<td width="10"><input type="radio" name="settore" value="altro"<% If settore_az="altro" Then response.write " checked"%>></td>
			<td width="100">&nbsp;Altro</td>
			<td class="righe" width="20"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>Attivit�</b>:&nbsp;</td>
			<td width="840"><input type="text" name="attivita" style="width:840px;" class="inse" maxlength="250" value="<%=attivita_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>N&deg; Addetti</b>:&nbsp;</td>
			<td width="170"><input type="text" name="n_addetti" style="width:170px;" class="inse" maxlength="50" value="<%=n_addetti_az%>"></td>
			<td class="righe" width="165"><b>di cui dipendenti T.P.</b>:&nbsp;</td>
			<td width="170"><input type="text" name="dipendenti_tp" style="width:170px;" class="inse" maxlength="50" value="<%=dipendenti_tp_az%>"></td>
			<td class="righe" width="165"><b>di cui dipendenti P.T.</b>:&nbsp;</td>
			<td width="170"><input type="text" name="dipendenti_pt" style="width:170px;" class="inse" maxlength="50" value="<%=dipendenti_pt_az%>"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>Fatturato</b>:&nbsp;</td>
			<td width="10"><input type="radio" name="fatturato" value="min_100"<% If fatturato_az="min_100" Then response.write " checked"%>></td>
			<td width="100">&nbsp;&lt;100</td>
			<td width="10"><input type="radio" name="fatturato" value="min_500"<% If fatturato_az="min_500" Then response.write " checked"%>></td>
			<td width="100">&nbsp;&lt;500</td>
			<td width="10"><input type="radio" name="fatturato" value="min_1000"<% If fatturato_az="min_1000" Then response.write " checked"%>></td>
			<td width="100">&nbsp;&lt;1000</td>
			<td width="10"><input type="radio" name="fatturato" value="mag_1000"<% If fatturato_az="mag_1000" Then response.write " checked"%>></td>
			<td width="100">&nbsp;&gt;1000</td>
			<td width="10"><input type="radio" name="fatturato" value="mag_2000"<% If fatturato_az="mag_2000" Then response.write " checked"%>></td>
			<td width="100">&nbsp;&gt;2000</td>
			<td class="righe" width="290"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe" width="150"><b>Grado di interesse</b>:&nbsp;</td>
			<td width="10"><input type="radio" name="interesse_gps" value="1"<% If interesse_gps_az="1" Then response.write " checked"%>></td>
			<td width="50">&nbsp;1</td>
			<td width="10"><input type="radio" name="interesse_gps" value="2"<% If interesse_gps_az="2" Then response.write " checked"%>></td>
			<td width="50">&nbsp;2</td>
			<td width="10"><input type="radio" name="interesse_gps" value="3"<% If interesse_gps_az="3" Then response.write " checked"%>></td>
			<td width="50">&nbsp;3</td>
			<td width="10"><input type="radio" name="interesse_gps" value="4"<% If interesse_gps_az="4" Then response.write " checked"%>></td>
			<td width="50">&nbsp;4</td>
			<td width="10"><input type="radio" name="interesse_gps" value="5"<% If interesse_gps_az="5" Then response.write " checked"%>></td>
			<td width="50">&nbsp;5</td>
			<td width="10"><input type="radio" name="interesse_gps" value="6"<% If interesse_gps_az="6" Then response.write " checked"%>></td>
			<td width="50">&nbsp;6</td>
			<td width="10"><input type="radio" name="interesse_gps" value="7"<% If interesse_gps_az="7" Then response.write " checked"%>></td>
			<td width="50">&nbsp;7</td>
			<td width="10"><input type="radio" name="interesse_gps" value="8"<% If interesse_gps_az="8" Then response.write " checked"%>></td>
			<td width="50">&nbsp;8</td>
			<td width="10"><input type="radio" name="interesse_gps" value="9"<% If interesse_gps_az="9" Then response.write " checked"%>></td>
			<td width="50">&nbsp;9</td>
			<td width="10"><input type="radio" name="interesse_gps" value="10"<% If interesse_gps_az="10" Then response.write " checked"%>></td>
			<td width="50">&nbsp;10</td>
			<td class="righe" width="180"></td>
		  </tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"><b>Attese da GPSimpresa</b>:</td>
			<td width="840"><textarea name="attese_gps" style="width:840px;height:100px;" class="inse_textarea"><%=attese_gps_az%></textarea></td>
		  </tr>
		</table>
		<% If id_az="" Then %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_001.gif" border="0"></td>
		  </tr>
		</table>
		<% else %>
		<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;margin-bottom:40px;">
		  <tr>
		    <td class="righe_textarea" width="150" valign="top"></td>
			<td width="840"><input type="image" src="img/save_002.gif" border="0"></td>
		  </tr>
		</table>
		<% End if %>
	</td>
  </tr>
</table>
</form>
	

</body>
</html>
<!-- #include file = "include/cnn_close.asp" -->