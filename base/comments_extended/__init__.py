from comments_extended.models import CommentWithTitle
from comments_extended.forms import CommentFormWithTitle

def get_model():
    return CommentWithTitle

def get_form():
    return CommentFormWithTitle