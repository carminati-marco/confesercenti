from django import forms
from django.contrib.comments.forms import CommentDetailsForm
from comments_extended.models import CommentWithTitle

class CommentFormWithTitle(CommentDetailsForm):
    title = forms.CharField(max_length=300)
    email = forms.EmailField(required = False)

    def get_comment_model(self):
        # Use our custom comment model instead of the built-in one.
        return CommentWithTitle

    def get_comment_create_data(self):
        # Use the data of the superclass, and add in the title field
        data = super(CommentFormWithTitle, self).get_comment_create_data()
        data['title'] = self.cleaned_data['title']
        return data
    
    class Meta:
        model = CommentWithTitle
        fields = ('title', 'comment',)