'''
Created on Apr 9, 2011

@author: 
'''
from django.contrib import admin
from conventions.models import Convention, ConventionType

from django import forms
from tinymce.widgets import TinyMCE

class ConventionAdminModelForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label = ('Testo'))

    class Meta:
        model = Convention


class ConventionAdmin(admin.ModelAdmin):    
    list_display = ('title','user', 'created_at', 'published', 'order', 'pub_date')
    search_fields = ['body', 'title']
    filter_horizontal = ('category', 'macrocategory',)
    raw_id_fields = ('documents',)
    
    form = ConventionAdminModelForm
    
    def save_model(self, request, obj, form, change):
        """ Autofill in author when blank on save models. """
        obj.user = request.user
        obj.save()
    # EndDef


admin.site.register(Convention, ConventionAdmin)
admin.site.register(ConventionType)
