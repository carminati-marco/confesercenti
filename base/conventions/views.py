from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from conventions.models import Convention



from django.http import Http404

from django.contrib.auth.decorators import login_required

from django.db.models import Q
from django.conf import settings

def listing(request, page=None):
    conventions_list = Convention.pubs_objects.all().order_by('type__name')

    return render_to_response('conventions/conventions_listing.html',
        {
         'conventions': conventions_list,
        },
        context_instance=RequestContext(request))
    
def convention_details(request, slug=None, page_id=None):
    conventions = get_object_or_404(Convention, slug=slug)
    
    ##nel caso in cui visits sia null
    if not conventions.visits:
        conventions.visits = 0
        
    conventions.visits = conventions.visits + 1
    conventions.save()
    
    if conventions.published != True:
        raise Http404
    
    return render_to_response('conventions/conventions_detail.html',
        {
         'convention': conventions,         
        },
        context_instance=RequestContext(request))
    
