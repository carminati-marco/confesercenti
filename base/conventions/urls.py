from django.conf.urls.defaults import *


urlpatterns = patterns('conventions.views',
    url(r'^$', 'listing', name='list-conventions'),
    url(r'^dettaglio/(?P<slug>[-\w]+)/$', 'convention_details', name='convention-details'),
        
)
