from django import http
from django.conf import settings
from django.core.paginator import Paginator, InvalidPage
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
#from livesettings import config_value
from documents.models import Document, Type
from documents.forms import DocumentForm

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.db.models import Q

import logging

document_id = settings.MODULES_DOCUMENT_ID
        
log = logging.getLogger('product.views.filters')


def home(request, 
         template='documents/documents_list.html'):
    
    
    ctx = RequestContext(request, {
        'documents' : Document.site_objects.filter(visible = True),
        'private_page' : False,
    })
    return render_to_response(template, context_instance=ctx)


def document_type(request, type_id, template='documents/documents_list.html'):
    
    type = get_object_or_404(Type, id = type_id)
        
    ctx = RequestContext(request, {
        'type' : type,
        'documents' : Document.site_objects.filter(visible = True,  
                                                   type__id = type_id),
        'private_page' : False,
        'type' : type,
    })
    return render_to_response(template, context_instance=ctx)

    
def urls(request, template='documents/documents_list.html'):
    
    
    ctx = RequestContext(request, {
        'documents' : Document.site_objects.filter(visible = True).filter(deleted = False),
    })
    return render_to_response(template, context_instance=ctx)

@login_required
def home_private(request,
                personal = False,
                template='documents/documents_list.html'):    
    ctx = None
    q = Q()
    
    search_text = request.GET.get('search_text', '')
    
    ##pagina personale
    if personal:
        q = Q(visible = False, deleted = False, parent = None, user = request.user, )
    ##pagina di ricerca (cerca anche nei documenti figli)
    elif search_text:
        q = Q(visible = False, deleted = False)
    ##home dei documenti (solo documenti padre)
    else:
        q = Q(visible = False, deleted = False, parent = None)
    
    if search_text:
        search_q = Q(description__icontains = search_text) | Q(title__icontains = search_text) | Q(tags__icontains = search_text)
        q = q & search_q    
    
    document_list = Document.site_objects.filter(q)
    
    if request.user.is_authenticated():    
        ctx = RequestContext(request, {
            'documents' : document_list,
            'private_page' : True,
            'personal' : personal,
            'search_text' : search_text,
        })
    return render_to_response(template, context_instance=ctx)


def module(request, template='documents/module_list.html'):    
    ctx = None
    
    
    ctx = RequestContext(request, {
        'documents' : Document.site_objects.filter(visible = True, type__id = document_id),
        'private_page' : True,
    })
    return render_to_response(template, context_instance=ctx)

@login_required
def home_private_manage(request, document_id_parent = None, document_id = None, 
                        form_class = DocumentForm,
                        template='documents/documents_manage.html'):
    """
    Inserimento/Modifica Documenti
    """    
    ctx = None  
    document = None  
    if document_id:
        document = Document.site_objects.get(id = document_id)
        
    print 'inizio'
    
    #if request.user.is_staff:    
        
    print 'prova'
    if request.method == "POST":
        print ('news_admin POST')
        if document:
            form = form_class(request.POST, request.FILES, instance = document)
            
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('documents_list_private'))

            else:
                print ('news_admin.errors : ' + unicode(form.errors))
                            
        else:
            form = form_class(request.POST, request.FILES)
        
            if form.is_valid():
                document_new = form.save(commit = False)
                document_new.user = request.user
                document_new.visible = False
                document_new.save()
                    
                ##aggiungo il sito.
                site = Site.objects.get_current()              
                document_new.sites.add(site)                    
                document_new.save()
            
                return HttpResponseRedirect(reverse('documents_list_private'))
                print ('salvato')
            else:
                print ('news_admin.errors : ' + unicode(form.errors))
    
    elif document:
        ##update.
        form = form_class(instance = document)
    else:
        ##new.
        if document_id_parent:
            form = form_class(initial = {'parent' : document_id_parent,})
        else:
            form = form_class()
            
        
    form.fields['parent'].queryset = Document.site_objects.filter(parent = None, visible = False)
    
    ctx = RequestContext(request, {
        'form' : form,
    })
    return render_to_response(template, context_instance=ctx)

@login_required
def home_private_delete(request, document_id,
                         template='documents/documents_delete.html'):    
    ctx = None
    
    document = Document.site_objects.get(id = document_id)
    document.deleted = True
    document.save()
    
    return HttpResponse('Eliminato')


@login_required
def home_private_detail(request, document_id,
                         template='documents/documents_detail.html'):    
    ctx = None
    
    document = Document.site_objects.get(id = document_id)
    ctx = RequestContext(request, {
            'document' : document,
        })
    return render_to_response(template, context_instance=ctx)