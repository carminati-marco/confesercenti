from django.conf.urls.defaults import *
#import product
#from signals_ahoy.signals import collect_urls

urlpatterns = patterns('documents.views',
#    (r'^/$',
#        'home', {}, 'documents_list'),
    url(r'^$', 'home', name='documents_list'),
    
    
    url(r'^tipo/(?P<type_id>\d+)/$', 'document_type', name='document_type'),
    
    
    url(r'^privati/$', 'home_private', name='documents_list_private'),
    url(r'^privati/miei/$', 'home_private',  { 'personal' : True }, 'documents_list_private_personal'),
    
    
    url(r'^privati/crea/$', 'home_private_manage', name='documents_list_private_add'),
    url(r'^privati/crea/(\d+)/$', 'home_private_manage', name='documents_list_private_add_with_id'),
    
    url(r'^privati/dettaglio/(?P<document_id>\d+)/$', 'home_private_detail', name='documents_list_private_detail'),
    url(r'^privati/modifica/(?P<document_id>\d+)/$', 'home_private_manage', name='documents_list_private_edit'),
    url(r'^privati/elimina/(?P<document_id>\d+)/$', 'home_private_delete', name='documents_list_private_delete'),
    
    
    url(r'^moduli/$', 'module', name='module_list'),
    
    
        
#    (r'^(?P<product_slug>[-\w]+)/prices/$', 
#        'get_price', {}, 'satchmo_product_prices'),
#    (r'^(?P<product_slug>[-\w]+)/price_detail/$', 
#        'get_price_detail', {}, 'satchmo_product_price_detail'),
)

