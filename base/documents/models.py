from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site, SiteManager
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from django.conf import settings

from datetime import date
from tagging.fields import TagField

"""
Tipo
"""
class Type(models.Model):
    name = models.CharField(max_length = 200, verbose_name = _('Nome'))
    slug = models.SlugField(blank=True, null=True)
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'))
    sites = models.ManyToManyField(Site, blank=True, null=True)
        
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.name
    
    def save(self, **kwargs): 
        self.slug = slugify(self.name)
        super(Type, self).save(**kwargs) 
    
    def get_absolute_url(self):
        return self.id
    
    class Meta:
        verbose_name = _("Tipo Documento")
        verbose_name_plural = _("Tipi Documenti")
    
    
"""
Documento
"""
class Document(models.Model):
    title = models.CharField(max_length = 200, verbose_name = _('Titolo'))
    slug = models.SlugField(blank=True, null=True)
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'))
    attachment = models.FileField(upload_to='documents/%Y/%m/%d', max_length=255, verbose_name = _('Allegato')) 
    insert_date = models.DateField(verbose_name = _('Data Inserimento'), default = date.today)
    type = models.ForeignKey(Type, blank=True, null=True, related_name='document_type', verbose_name = _('Tipo'))
    visible = models.BooleanField(default=True, verbose_name = _('Visibile'))
    deleted = models.BooleanField(default= False, verbose_name = _('Eliminato'))
    parent = models.ForeignKey('self', blank=True, null=True, related_name='child', verbose_name = _('Documento Originale'))
    user = models.ForeignKey(User, blank=True, null=True)   
    sites = models.ManyToManyField(Site)
    
    tags = TagField(blank=True, null=True)
    
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.title

    def save(self, **kwargs): 
        self.slug = slugify(self.title)
        
        if not self.insert_date:
            self.insert_date = date.today()
        
        super(Document, self).save(**kwargs)
    
    def get_absolute_url(self):
        return self.attachment.url
    
    def get_detail_url(self):
        return reverse('documents_list_private_detail', args=(self.id,))
        #return ("documents_list_private_detail", [self.id])
        #return self.attachment.url
    
    class Meta:
        ordering = ['-insert_date'] 
        verbose_name = _("Documento")
        verbose_name_plural = _("Documenti")


class ModuleDocument(models.Model):
    title = models.CharField(max_length = 200, verbose_name = _('Titolo'))
    slug = models.SlugField(blank=True, null=True)
    description = models.CharField(max_length = 200, verbose_name = _('Descrizione'))
    attachment_1 = models.FileField(upload_to='documents/%Y/%m/%d', max_length=255, verbose_name = _('CCNL Terziario'), blank = True, null = True)
    attachment_2 = models.FileField(upload_to='documents/%Y/%m/%d', max_length=255, verbose_name = _('CCNL Turismo'), blank = True, null = True) 
    insert_date = models.DateField(verbose_name = _('Data Inserimento'))
    type = models.ForeignKey(Type, blank=True, null=True, related_name='module_document_type', verbose_name = _('Tipo'))
    visible = models.BooleanField(default=True, verbose_name = _('Visibile'))
    
        
    sites = models.ManyToManyField(Site)
    
    site_objects = SiteManager()
    
    def __unicode__(self):
        return self.title

    def save(self, **kwargs): 
        self.slug = slugify(self.title)
        super(ModuleDocument, self).save(**kwargs)
    
    
    class Meta:
        ordering = ['-insert_date'] 
        verbose_name = _("Documento CCNL")
        verbose_name_plural = _("Documenti CCNL")    