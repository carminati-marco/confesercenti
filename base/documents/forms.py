from django import forms
from models import Document
from django.utils.translation import ugettext_lazy as _

from tinymce.widgets import TinyMCE

class DocumentForm(forms.ModelForm):

    class Meta:
        model = Document
        exclude = ('slug', 'insert_date', 'type', 'sites', 'visible', 'user', 'deleted' )