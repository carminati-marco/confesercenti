from django.contrib import admin
from django import forms
from models import Type, Document, ModuleDocument

admin.site.register(Type)

class DocumentAdmin(admin.ModelAdmin):
    list_display = ('title','type', 'insert_date',)
    search_fields = ['description', 'title']
    ordering = ('-insert_date',)
    list_filter = ('type',)    
    
    

admin.site.register(Document, DocumentAdmin)
admin.site.register(ModuleDocument)
