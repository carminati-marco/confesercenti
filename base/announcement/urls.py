from django.conf.urls.defaults import *


urlpatterns = patterns('announcement.views',
    url(r'^$', 'listing', name='list-announcement'),
    url(r'^(?P<page>\d+)/$', 'listing', name='list-announcement'),
    url(r'^aperti/$', 'listing', {'open': True},'list-announcement-open'),



    url(r'^finanza-agevolata/$', 'listing_nologin', {'open': True, 'template_name' : 'announcement/announcement_listing_finanza_agevolata.html', },'list-announcement-open'),
    url(r'^finanza-agevolata/(?P<slug>[-\w]+)/$', 'announce_detail', {'template_name' : 'announcement/announcement_detail_finanza_agevolata.html',}, 'detail-announcement-finanza-agevolata'),

    url(r'^(?P<slug>[-\w]+)/$', 'announce_detail', name='detail-announcement'),

)
