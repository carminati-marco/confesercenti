

from django import forms
from .models import Announcement
from django.utils.translation import ugettext_lazy as _

from tinymce.widgets import TinyMCE


class AnnouncementAdminModelForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Breve descrizione')
    headofservice = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Responsabile servizio')
    promoter = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Ente Promotore')
    beneficiaries = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Destinatari')
    investments = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Investimenti Ammissibili')
    documentation = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Documentazione Richiesta')
    cost = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Costo del Servizio')
    internal_communication = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Comunicazione Interna')
    external_communication = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Comunicazione Esterna')
    text_ban = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30},), label='Testo Bando')

    class Meta:
        model = Announcement
