'''
Created on Apr 9, 2011

@author: suca
'''
from django.contrib import admin
from announcement.models import Announcement, AnnouncementType
from categories.models import SubCategory
from django.contrib.sites.models import Site
from .forms import AnnouncementAdminModelForm


class AnnouncementAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title','user', 'created_at', 'published')
    search_fields = ['body', 'title']
    ordering = ('-created_at',)
    list_filter = ('site', 'created_at', 'updated_at', 'published')
    filter_horizontal = ('category', 'macrocategory',)
    raw_id_fields = ('user','documents',)
    
    fieldsets = [
        ('Generale', {'fields': ['title', 'slug'],}),
        ('Pubblicato', {'fields': ['published', 'pub_date', 'expiry_at', 'image', 'type']}),
        ('Sito', {'fields': ['site', 'macrocategory', 'category', 'documents']}),
        ('Finanza Agevolata', {'fields': ['body', 'link', 'phone', 'email', 'fax']}),
        ('Dettaglio', {'fields': ['headofservice', 'promoter', 'beneficiaries', 'investments', 'documentation',
                                  'cost', 'internal_communication', 'external_communication', 'text_ban']})

    ]
    form = AnnouncementAdminModelForm
    
    def save_model(self, request, obj, form, change):
        """ Autofill in author when blank on save models. """
        obj.user = request.user
        obj.save()
        
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "category":
            kwargs["queryset"] = SubCategory.objects.filter(sites = Site.objects.get_current())
        return super(AnnouncementAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
    # EndDef


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(AnnouncementType)
