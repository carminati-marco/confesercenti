# encoding: utf-8
from datetime import datetime, timedelta, date

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

from categories.models import Category, SubCategory
from common.thumbs import ImageWithThumbsField
from documents.models import Document

from django.conf import settings
my_logger = settings.MY_LOGGER

class PublishedAnnouncementManager(models.Manager):
    def all(self):
        return self.filter()

    def filter(self, *args, **kwargs):
        tomorrow = date.today() + timedelta(days=1)
        return super(PublishedAnnouncementManager, self).filter(published=True, expiry_at__gte=tomorrow).filter(*args, **kwargs)

class AnnouncementType(models.Model):
    name = models.CharField(max_length=255, verbose_name = _('Nome'))
    
    def __unicode__(self):
        return u"%s" % self.name
    
    class Meta:
        verbose_name = _("Tipo bando")
        verbose_name_plural = _("Tipi bando")
        

class Announcement(models.Model):
    #pubs_objects = PublishedNewsManager()
    objects = models.Manager()
    on_site = CurrentSiteManager()
    pubs_objects = PublishedAnnouncementManager()
    
    # Optional
    title = models.CharField(max_length=255, verbose_name = _('Titolo')) 
    slug = models.SlugField(unique=True)
    body = models.TextField(verbose_name = _('Breve descrizione'))

    ruler = models.TextField(verbose_name = _('Gestore'))
    recipients = models.TextField(verbose_name = _('Destinatari'))
    facilities = models.TextField(verbose_name = _('Agevolazioni'))
    billing = models.TextField(verbose_name = _('Fatturazione'))
    advertising = models.TextField(verbose_name = _('Comunicazione'))
    link = models.CharField(max_length=255, verbose_name = _('Link'))
    phone = models.CharField(max_length=20, verbose_name = ('Telefono'), blank = True, null=True)
    email = models.CharField(max_length=50, verbose_name = ('E-mail'), blank = True, null=True)
    fax = models.CharField(max_length=20, verbose_name = ('Fax'), blank = True, null=True)

    published = models.BooleanField(default=False, verbose_name = _('Pubblicato'))
    
    pub_date = models.DateTimeField(verbose_name = _('Data apertura'), default = datetime.now)
    expiry_at = models.DateTimeField(verbose_name = _('Data di scadenza'), default = datetime.now)
    site = models.ManyToManyField(Site, verbose_name = _('Sito'))
    macrocategory = models.ManyToManyField(Category, verbose_name = _('Macro Categoria'), blank = True, null = True)
    category = models.ManyToManyField(SubCategory, verbose_name = _('Categoria'), blank = True, null = True)
    
    type = models.ForeignKey(AnnouncementType, verbose_name = _('Tipo Bando'), blank = True, null = True)
    
    visits = models.IntegerField(default = 0, verbose_name = _('Numero visite'))
    order = models.IntegerField(default = 0, verbose_name = _('Posizione Bando'))
    
    # Automatic, verbose_name = _('Categoria')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User)

    documents = models.ManyToManyField(Document, blank = True, null = True)
    
    image =  ImageWithThumbsField(blank=True, null=True, upload_to='news/images', sizes=((125,125),(200,200),(500,220)),
                                  verbose_name = _('Immagine'))

    headofservice = models.TextField(verbose_name = _('Responsabile servizio'))
    promoter = models.TextField(verbose_name = _('Ente Promotore'))
    beneficiaries = models.TextField(verbose_name = _('Destinatari'))
    investments = models.TextField(verbose_name = _('Investimenti Ammissibili'))
    documentation = models.TextField(verbose_name = _('Documentazione Richiesta'))
    cost = models.TextField(verbose_name = _('Costo del Servizio'))
    internal_communication = models.TextField(verbose_name = _('Comunicazione Interna'))
    external_communication = models.TextField(verbose_name = _('Comunicazione Esterna'))
    text_ban = models.TextField(verbose_name = _('Testo Bando'))
    
    def __unicode__(self):
        return u"%s" % self.title
    
    def get_absolute_url(self):
        return "/bandi/%s/" % self.slug

    def is_closed(self):
        tomorrow = date.today() + timedelta(days=1)
        return self.expiry_at.date() < tomorrow
    
    class Meta:
        ordering = ['-pub_date', '-order',]
        verbose_name = _("Bando")
        verbose_name_plural = _("Bandi")

