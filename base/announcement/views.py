from datetime import date, timedelta
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import Http404
from django.contrib.auth.decorators import login_required

from announcement.models import Announcement
from news.settings import NEWS_PER_PAGE, NUMBER_TRUNCATE_WORDS


@login_required
def listing(request, page=None, open=False, template_name='announcement/announcement_listing.html'):
    announcement_list = Announcement.on_site.all()

    # bandi aperti
    if open:
        # aggiungo il filtro per annunci expiry_at > domani
        tomorrow = date.today() + timedelta(days=1)
        announcement_list = announcement_list.filter(expiry_at__gte=tomorrow)
    else:
        if not request.user.has_module_perms('announcement'):
            raise Http404



    #paginator = Paginator(announcement_list, NEWS_PER_PAGE)
    
    # Make sure page request is an int. If not, deliver first page.
    if page is None:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    #try:
    #    announcement_list = paginator.page(page)
    #except (EmptyPage, InvalidPage):
    #    announcement_list = paginator.page(paginator.num_pages)

    return render_to_response(template_name,
        {
         'announcement_list': announcement_list,
         'words': NUMBER_TRUNCATE_WORDS,
        },
        context_instance=RequestContext(request))

def listing_nologin(request, page=None, open=False, template_name='announcement/announcement_listing.html'):
    announcement_list = Announcement.on_site.all()

    # bandi aperti
    if open:
        # aggiungo il filtro per annunci expiry_at > domani
        tomorrow = date.today() + timedelta(days=1)
        announcement_list = announcement_list.filter(expiry_at__gte=tomorrow)
    else:
        if not request.user.has_module_perms('announcement'):
            raise Http404



    #paginator = Paginator(announcement_list, NEWS_PER_PAGE)

    # Make sure page request is an int. If not, deliver first page.
    if page is None:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    #try:
    #    announcement_list = paginator.page(page)
    #except (EmptyPage, InvalidPage):
    #    announcement_list = paginator.page(paginator.num_pages)

    return render_to_response(template_name,
        {
         'announcement_list': announcement_list,
         'words': NUMBER_TRUNCATE_WORDS,
        },
        context_instance=RequestContext(request))

def announce_detail(request, slug=None, template_name='announcement/announcement_detail.html'):
    announce = get_object_or_404(Announcement, slug=slug)
    
    ##nel caso in cui visits sia null
    if announce.visits == None:
        announce.visits = 0
        
    announce.visits = announce.visits + 1
    announce.save()
    
    if announce.published != True:
        raise Http404
    
    return render_to_response(template_name,
        {
         'announce': announce,
        },
        context_instance=RequestContext(request))